import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state={
    name: '',
    nameValidation: false
  }
  
  onChangeValue = (value)=>{
    this.setState({name: value});
  }

  componentDidMount(){
    this.test()
  }

  test = ()=>{
    window.addEventListener('click', ()=>{
      console.log('clicando')
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <section>
        <label>Minha label: </label>
        <input 
          style={{marginTop: 25}} 
          type='text' value={this.state.name} 
          onChange={(event)=> this.onChangeValue(event.target.value)}
          placeholder='Digit your name'
          maxLength={4}
        />
        {
          this.state.nameValidation
          ? <input type='submit' alt='Enviar'/>
          : <p>Essa consicao é falsa</p>
        }
        </section>
      </div>

    );
  }
}

export default App;
