import React from "react";
import { View, ActivityIndicator } from "react-native";
import { BaseStyle } from "../../styles/base";

export default function Load() {
  return (
    <View style={BaseStyle.loadingContainer}>
      <ActivityIndicator size={60} color="#EE6E73" />
    </View>
  );
}
