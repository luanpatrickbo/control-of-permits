import React, { Component } from "react";
import { View, Picker, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { setValue, clearValues } from "../../actions/valuesActions";
import { editAdres } from "../../actions/clientActions";
import { BaseStyle } from "../../styles/base";
import States from "../../utils/states";

class SelectState extends Component {
  state = {
    states: "",
    city: ""
  };

  componentDidMount() {
    const stateProps = this.props.state;
    const city = this.props.navigation.getParam("city", "city-companie");
    this.setState({ city });
    this.setState({ states: stateProps });
  }

  EditClient = async () => {
    const { editAdres, isConnect, navigation } = this.props;

    const id = navigation.getParam("id", "adress-id");
    const number = navigation.getParam("number", "number-companie");
    const adress = navigation.getParam("adress", "adress-companie");
    const city = navigation.getParam("city", "city-companie");
    const cep = navigation.getParam("cep", "cep-companie");
    const neighborhood = navigation.getParam(
      "neighborhood",
      "neighborhood-companie"
    );
    const { states } = this.state;

    try {
      if (isConnect) {
        await editAdres(id, {
          adress,
          neighborhood,
          cep,
          city,
          number,
          states
        });
      }
      navigation.navigate("StackInfoClient");
    } catch (error) {
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { states } = this.state;
    const { navigation, EditState } = this.props;
    const number = navigation.getParam("number", "number-companie");
    const adress = navigation.getParam("adress", "adress-companie");
    const city = navigation.getParam("city", "city-companie");
    const cep = navigation.getParam("cep", "cep-companie");
    const neighborhood = navigation.getParam(
      "neighborhood",
      "neighborhood-companie"
    );
    // Original Atributes

    const EditAdress = navigation.getParam("EditAdress", "EditAdress-companie");
    const EditNumber = navigation.getParam("EditNumber", "EditNumber-companie");
    const EditCep = navigation.getParam("EditCep", "EditCep-companie");
    const EditCity = navigation.getParam("EditCity", "EditCity-companie");
    const EditNeighborhood = navigation.getParam(
      "EditNeighborhood",
      "EditNeighborhood-companie"
    );

    return (
      <View style={BaseStyle.formContainer}>
        <View style={BaseStyle.pickerContainer}>
          <Picker
            style={{ padding: 10, backgroundColor: "#F7F7F7" }}
            selectedValue={states}
            onValueChange={value => this.setState({ states: value })}
          >
            {States.map(item => (
              <Picker.Item key={item} label={item} value={item} />
            ))}
          </Picker>
        </View>
        <View style={BaseStyle.buttonContainer}>
          <TouchableOpacity
            disabled={
              number !== EditNumber ||
              neighborhood !== EditNeighborhood ||
              adress !== EditAdress ||
              city !== EditCity ||
              cep.replace(/\D/g, "") !== EditCep.replace(/\D/g, "") ||
              states !== EditState
                ? false
                : true
            }
            onPress={() => this.EditClient()}
          >
            <Text
              style={
                number !== EditNumber ||
                neighborhood !== EditNeighborhood ||
                adress !== EditAdress ||
                city !== EditCity ||
                cep.replace(/\D/g, "") !== EditCep.replace(/\D/g, "") ||
                states !== EditState
                  ? BaseStyle.button
                  : [BaseStyle.button, { backgroundColor: "#D46266" }]
              }
            >
              Editar
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { adress, number, cep, city } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    states: state.register.states,
    adress,
    cep,
    city,
    number,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    editAdres: (id, values) => dispatch(editAdres(id, values)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectState);
