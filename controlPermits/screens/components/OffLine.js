import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import { BaseStyle } from "../../styles/base";
import { OfflineStyle } from "../../styles/offline";
import { connect } from "react-redux";

const Offline = ({ isConnect, children }) => {
  return (
    <View style={{ flex: 1 }}>
      {isConnect ? (
        children
      ) : (
        <View style={BaseStyle.container}>
          <View style={OfflineStyle.textContainer}>
            <Feather name="alert-triangle" size={80} color="#4a494a" />
            <Text style={OfflineStyle.textColor}>Verifique sua conexão</Text>
            <TouchableOpacity>
              <Text />
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  return {
    isConnect: isConnect
  };
};

export default connect(mapStateToProps)(Offline);
