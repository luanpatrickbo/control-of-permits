import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { setValue } from "../../actions/valuesActions";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "./TitleRegister";
import ArrowBack from "./ArrowBack";
import OffLineForms from "../components/OffLineForms";

class AnnotationForm extends Component {
  state = {
    annotationValidation: false
  };

  onValidationAnnotation = value => {
    this.props.setValue("annotation", value);
    const express = /[a-zA-Z0-9]{1,150}/;
    const testExpress = new RegExp(express);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ annotationValidation: true });
    } else {
      this.setState({ annotationValidation: false });
    }
  };

  render() {
    const { annotationValidation } = this.state;

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Adicione uma anotação para o cliente" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            placeholder="Descrição"
            placeholderTextColor={BaseStyle.placeholderStyle}
            style={BaseStyle.input}
            underlineColorAndroid="transparent"
            multiline={true}
            numberOfLines={4}
            autoCorrect={false}
            maxLength={150}
            value={this.props.annotation}
            onChangeText={value => this.onValidationAnnotation(value)}
          />
          <View style={BaseStyle.buttonContainer}>
            {annotationValidation ? (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("StackPermit")}
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    annotation: state.register.annotation
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnnotationForm);
