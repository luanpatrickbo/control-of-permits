import React from "react";
import { View, TouchableOpacity, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import PropTypes from "prop-types";

function ArrowBack(props) {
  return (
    <View style={{ flexDirection: "row", marginTop: 40 }}>
      <View style={{ justifyContent: "flex-start", marginLeft: 20 }}>
        <TouchableOpacity
          hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
          onPress={() => props.navigation.goBack()}
        >
          <View>
            <Ionicons
              name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
              size={Platform.OS === "ios" ? 40 : 25}
              color="#4a494a"
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
export default ArrowBack;

ArrowBack.prototype = {
  navigation: PropTypes.object.isRequired
};
