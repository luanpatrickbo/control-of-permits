import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";

const OffLineForm = ({ isConnect }) => {
  return (
    <View>
      {!isConnect ? (
        <View
          style={{
            height: 30,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#2F6DF2"
          }}
        >
          <Text style={{ fontSize: 15, color: "#F7F7F7" }}>Sem Conexão</Text>
        </View>
      ) : null}
    </View>
  );
};

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  return {
    isConnect: isConnect
  };
};

export default connect(mapStateToProps)(OffLineForm);
