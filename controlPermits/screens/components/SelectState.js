import React, { Component } from "react";
import {
  View,
  Picker,
  TouchableOpacity,
  Text,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { setValue } from "../../actions/valuesActions";
import { BaseStyle } from "../../styles/base";
import States from "../../utils/states";

class SelectState extends Component {
  state = {
    city_validation: false
  };

  onValidationCity = value => {
    this.props.setValue("city", value);
    const expression = /[a-zA-Z]{4,35}/;
    const testExpress = new RegExp(expression);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ city_validation: true });
    } else {
      this.setState({ city_validation: false });
    }
  };

  render() {
    const { states, city } = this.props;
    return (
      <KeyboardAvoidingView style={BaseStyle.formContainer} behavior="padding">
        <TextInput
          style={BaseStyle.input}
          placeholder="Cidade"
          placeholderTextColor={BaseStyle.placeholderStyle}
          autoCorrect={false}
          underlineColorAndroid="transparent"
          maxLength={35}
          value={city}
          onChangeText={value => this.onValidationCity(value)}
        />
        <View style={[BaseStyle.pickerContainer, { marginTop: 20 }]}>
          <Picker
            style={{ padding: 10, backgroundColor: "#F7F7F7" }}
            selectedValue={states}
            onValueChange={value => this.props.setValue("states", value)}
          >
            {States.map(item => (
              <Picker.Item key={item} label={item} value={item} />
            ))}
          </Picker>
        </View>
        <View style={BaseStyle.buttonContainer}>
          <TouchableOpacity
            disabled={this.state.city_validation ? false : true}
            onPress={() => this.props.navigation.navigate("StackPhone")}
          >
            <Text
              style={
                this.state.city_validation
                  ? BaseStyle.button
                  : [BaseStyle.button, { backgroundColor: "#D46266" }]
              }
            >
              Avançar
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  const { states, city } = state.register;
  return {
    city,
    states
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectState);
