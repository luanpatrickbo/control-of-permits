import React, { Component } from "react";
import {
  View,
  Text,
  Picker,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { BaseStyle } from "../../styles/base";
import { getAllKinds } from "../../actions/kindActions";
import { setValue } from "../../actions/valuesActions";

class TypeOfLicenses extends Component {
  state = {
    kinds: "",
    date: new Date(),
    kindId: 0,
    isVisible: false
  };

  componentDidMount() {
    this.props.loadKinds();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.kinds !== this.props.kinds) {
      const selectedValue = this.props.kinds[0].id;
      this.props.setValue("kind", selectedValue);
    }
  }

  handlePicker = date => {
    this.setState({
      isVisible: false,
      date
    });
  };

  hidePicker = () => {
    this.setState({ isVisible: false });
  };

  showPicker = () => {
    this.setState({ isVisible: true });
  };

  render() {
    const { isVisible } = this.state;
    const maximumDate = new Date();
    maximumDate.setFullYear(maximumDate.getFullYear() + 2);
    const minimumDate = new Date();
    minimumDate.setMonth(minimumDate.getMonth() - 6);

    console.log(this.props.kind);
    return (
      <View style={BaseStyle.formContainer}>
        <View
          style={[
            BaseStyle.pickerContainer,
            { marginBottom: 20, marginLeft: 20, marginRight: 20 }
          ]}
        >
          <Picker
            style={{ padding: 10, backgroundColor: "#F7F7F7" }}
            onValueChange={value => this.props.setValue("kind", value)}
            selectedValue={this.props.kind}
          >
            {this.props.kinds.map(kind => (
              <Picker.Item key={kind.id} label={kind.kind} value={kind.id} />
            ))}
          </Picker>
        </View>
        <TouchableWithoutFeedback onPress={() => this.showPicker()}>
          <View>
            <Text style={BaseStyle.datePicker}>
              {moment(this.state.date).format("DD/MM/YYYY")}
            </Text>
          </View>
        </TouchableWithoutFeedback>

        <DateTimePicker
          isVisible={isVisible}
          onConfirm={this.handlePicker}
          onCancel={this.hidePicker}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
          date={this.state.date}
        />
        <View style={BaseStyle.buttonContainer}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("StackPermit2", {
                date: this.state.date
              })
            }
          >
            <Text style={BaseStyle.button}>Avançar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

TypeOfLicenses.propTypes = {
  navigation: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  const { kinds } = state.kind;
  const { kind } = state.register;

  return {
    kinds,
    isConnect,
    kind
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadKinds: () => dispatch(getAllKinds()),
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TypeOfLicenses);
