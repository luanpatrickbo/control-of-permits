import React from "react";
import { View, Text } from "react-native";
import { BaseStyle } from "../../styles/base";
import PropTypes from "prop-types";

function TitleRegister({ title }) {
  return (
    <View style={[BaseStyle.titleRegister, { marginTop: 30 }]}>
      <Text style={BaseStyle.title}>{title}</Text>
    </View>
  );
}

export default TitleRegister;

TitleRegister.prototype = {
  title: PropTypes.string.isRequired
};
