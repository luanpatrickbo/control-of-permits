import React, {Component} from 'react'
import {View, Text, Image} from 'react-native'
import { BaseStyle } from '../../styles/base'

class Header extends Component{
  render(){
    return(
      <View style={BaseStyle.header}>
        <Text style={BaseStyle.textHeader}>Alvarás Control</Text>
      </View>
    )
  }
}

export default Header
