import React, { Component } from "react";
import { View, TouchableOpacity, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { clearValues } from "../../actions/valuesActions";
import { connect } from "react-redux";

class ArrowBackwithExit extends Component {
  onClearValues = () => {
    const { route } = this.props;
    this.props.clearValues();
    this.props.navigation.navigate(route);
  };

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          marginTop: 40
        }}
      >
        <View style={{ justifyContent: "flex-start", marginLeft: 20 }}>
          <TouchableOpacity
            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
            onPress={() => this.props.navigation.goBack()}
          >
            <View>
              <Ionicons
                name={
                  Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"
                }
                size={Platform.OS === "ios" ? 40 : 25}
                color="#4a494a"
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ marginLeft: 290 }}>
          <TouchableOpacity
            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
            onPress={() => this.onClearValues()}
          >
            <View>
              <Ionicons name="md-close" size={25} color="#4a494a" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ArrowBackwithExit);
