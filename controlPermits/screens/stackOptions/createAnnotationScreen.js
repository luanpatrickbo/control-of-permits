import React, { Component } from "react";
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { BaseStyle } from "../../styles/base";
import ArrowBack from "../components/ArrowBack";
import TitleRegister from "../components/TitleRegister";
import { setValue, clearValues } from "../../actions/valuesActions";
import { postAnnotation } from "../../actions/annotationActions";
import OffLineForms from "../components/OffLineForms";

class CreateAnnotationScreen extends Component {
  state = {
    annotationValidation: false
  };

  onValidationAnnotation = value => {
    this.props.setValue("annotation", value);
    const testExpress = new RegExp(/[a-zA-Z0-9]{4,150}/);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ annotationValidation: true });
    } else {
      this.setState({ annotationValidation: false });
    }
  };

  onCreateAnnotation = async description => {
    const { createAnnotation, clearValues, navigation, isConnect } = this.props;

    try {
      if (isConnect) {
        createAnnotation(description);
        await clearValues();
        navigation.navigate("AnnotationScreen");
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { annotationValidation } = this.state;
    const { annotation } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Adicione uma anotação" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            placeholder="Descrição"
            placeholderTextColor={BaseStyle.placeholderStyle}
            style={BaseStyle.input}
            underlineColorAndroid="transparent"
            multiline={true}
            numberOfLines={4}
            autoCorrect={false}
            maxLength={150}
            value={annotation}
            onChangeText={value => this.onValidationAnnotation(value)}
          />
          <View style={BaseStyle.buttonContainer}>
            {annotationValidation ? (
              <TouchableOpacity
                onPress={() => this.onCreateAnnotation(annotation)}
              >
                <Text style={BaseStyle.button}>Cadastrar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Cadastrar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { annotation } = state.register;
  const { isConnect } = state.setNetInfo;

  return {
    annotation: annotation,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues()),
    createAnnotation: description => dispatch(postAnnotation(description))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateAnnotationScreen);
