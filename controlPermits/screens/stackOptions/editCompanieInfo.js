import React, { PureComponent } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { BaseStyle } from "../../styles/base";
import { setValue, clearValues } from "../../actions/valuesActions";
import Api from "../../utils/api";
import TitleRegister from "../components/TitleRegister";
import ArrowBack from "../components/ArrowBack";
import OffLineForm from "../components/OffLineForms";

class SocialNameScreen extends PureComponent {
  state = {
    check_social: false,
    check_fancy: false,
    loading: false,
    socialName: "",
    fancyName: ""
  };

  componentDidMount() {
    const socialName = this.props.navigation.getParam(
      "social_name",
      "social_name-companie"
    );
    const fancyName = this.props.navigation.getParam(
      "fancy_name",
      "fancy_name-companie"
    );

    this.setState({ socialName, fancyName });
  }
  clearValues = async () => {
    const { navigation, clearValues } = this.props;
    await clearValues();
    navigation.goBack();
  };

  checkParams = async () => {
    const { isConnect, navigation } = this.props;
    const { socialName, fancyName } = this.state;
    const id = navigation.getParam("id", "client-id");
    const cnpj = this.props.navigation.getParam("cnpj", "cnpj-companie");
    const responsible_name = this.props.navigation.getParam(
      "responsible_name",
      "client-responsibleName"
    );

    this.setState({ loading: true });

    try {
      if (isConnect) {
        const result = await Promise.all([
          Api.checkSocialName(socialName),
          Api.checkFancyName(fancyName)
        ]);

        this.setState({ check_social: result[0], check_fancy: result[1] });
        if (result[0] === false || result[1] === false) {
          this.props.navigation.navigate("StackEditCompanieCnpj", {
            id,
            socialName,
            fancyName,
            cnpj,
            responsible_name
          });
        }
      }

      this.setState({ loading: false });
    } catch (error) {
      alert("tente novamente mais tarde");
      this.setState({ loading: false });
    }
  };

  render() {
    const {
      loading,
      socialName,
      fancyName,
      check_fancy,
      check_social
    } = this.state;

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title={"Edite a razão social ou nome fantasia"} />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {check_fancy && check_social ? (
            <View style={BaseStyle.textValidationContainer}>
              <Text style={BaseStyle.textValidation}>
                Razão social ou nome fantasia já cadastrados
              </Text>
            </View>
          ) : null}
          <TextInput
            value={socialName}
            onChangeText={val => this.setState({ socialName: val })}
            style={BaseStyle.input}
            placeholder="Razão social"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={50}
          />
          <TextInput
            value={fancyName}
            onChangeText={val => this.setState({ fancyName: val })}
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder="Nome Fantasia"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={50}
          />
          <View style={BaseStyle.buttonContainer}>
            {socialName.trim().length >= 4 && fancyName.trim().length >= 4 ? (
              <TouchableOpacity onPress={() => this.checkParams()}>
                <View>
                  {loading ? (
                    <ActivityIndicator size={40} color={"#EE6E73"} />
                  ) : (
                    <Text style={BaseStyle.button}>Avançar</Text>
                  )}
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForm />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { socialName, fancyName } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    socialName: socialName,
    fancyName: fancyName,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialNameScreen);
