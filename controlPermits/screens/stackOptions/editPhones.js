import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { TextInputMask } from "react-native-masked-text";
import { setValue, clearValues } from "../../actions/valuesActions";
import { editPhones } from "../../actions/clientActions";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "../components/TitleRegister";
import ArrowBack from "../components/ArrowBack";
import OffLineForms from "../components/OffLineForms";

class EditPhones extends Component {
  state = {
    cellPhone: "",
    phone: ""
  };

  componentDidMount() {
    const number = this.props.navigation.getParam("numbers", "phones-numbers");
    this.setState({ cellPhone: number[1], phone: number[0] });
  }

  editPhone = async () => {
    const { navigation, editPhones, isConnect } = this.props;
    const { phone, cellPhone } = this.state;
    const phoneId = navigation.getParam("ids", "phone-ids");
    const id = navigation.getParam("id", "client-id");
    const number = this.props.navigation.getParam("numbers", "phones-numbers");
    const number0 = number[0];
    const number1 = number[1];

    try {
      if (isConnect && (phone !== number0 || cellPhone !== number1)) {
        editPhones(id, phoneId, {
          phone,
          cellPhone
        });
      }
      navigation.goBack();
    } catch (error) {
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { phone, cellPhone } = this.state;
    const number = this.props.navigation.getParam("numbers", "phones-numbers");
    const number0 = number[0];
    const number1 = number[1];

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Edite os telefones" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInputMask
            value={phone}
            onChangeText={value => this.setState({ phone: value })}
            style={BaseStyle.input}
            placeholder={`Digite o telefone ou celular`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            maxLength={15}
            autoCorrect={false}
            keyboardType="numeric"
            type={"cel-phone"}
          />
          <TextInputMask
            value={cellPhone}
            onChangeText={value => this.setState({ cellPhone: value })}
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder={`Digite o Telefone ou celular`}
            underlineColorAndroid="transparent"
            placeholderTextColor={BaseStyle.placeholderStyle}
            maxLength={15}
            keyboardType="numeric"
            type={"cel-phone"}
          />
          <View style={BaseStyle.buttonContainer}>
            {phone.trim().replace(/\D/g, "").length >= 10 &&
            cellPhone.trim().replace(/\D/g, "").length >= 10 &&
            (phone.replace(/\D/g, "") !== number0 ||
              cellPhone.replace(/\D/g, "") !== number1) &&
            phone.replace(/\D/g, "") !== cellPhone.replace(/\D/g, "") ? (
              <TouchableOpacity onPress={() => this.editPhone()}>
                <Text style={BaseStyle.button}>Editar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Editar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { phone, cellPhone } = state.register;
  const { isConnect } = state.setNetInfo;

  return {
    phone,
    cellPhone,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    editPhones: (id, phoneIds, values) => {
      dispatch(editPhones(id, phoneIds, values));
    },
    clearValues: () => clearValues()
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPhones);
