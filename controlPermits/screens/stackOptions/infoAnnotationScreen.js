import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  ScrollView,
  Alert
} from "react-native";
import {
  getAnnotation,
  removeAnnotation
} from "../../actions/annotationActions";
import { connect } from "react-redux";
import { BaseStyle } from "../../styles/base";
import {
  Ionicons,
  EvilIcons,
  MaterialCommunityIcons
} from "@expo/vector-icons";
import Load from "../components/Loading";
import OffLine from "../components/OffLine";

class InfoAnnotationScreen extends Component {
  state = {
    load: true
  };

  async componentDidMount() {
    const { navigation, loadAnnotation, isConnect } = this.props;
    const idAnnotation = navigation.getParam("idAnnotation", "annotationId");

    if (isConnect) {
      await loadAnnotation(idAnnotation);
    }

    this.setState({ load: false });
  }

  onDeleteAnnotation = () => {
    const { navigation, deleteAnnotation } = this.props;
    const idAnnotation = navigation.getParam("idAnnotation", "annotationId");
    deleteAnnotation(idAnnotation);
    navigation.goBack();
  };

  askAnnotation = () => {
    Alert.alert(
      "Você Realmente deseja excluir esta anotação?",
      "Todos os dados da anotação serão perdidos",
      [
        {
          text: "Cancelar",
          style: "cancel"
        },
        {
          text: "Sim, desejo excluir",
          onPress: () => this.onDeleteAnnotation()
        }
      ],
      { cancelable: false }
    );
  };

  componentDidUpdate(prevProps) {
    const idAnnotation = this.props.navigation.getParam(
      "idAnnotation",
      "annotationId"
    );

    const { isConnect, loadAnnotation } = this.props;
    if (
      JSON.stringify(prevProps.annotations) !==
      JSON.stringify(this.props.annotations)
    ) {
      loadAnnotation(idAnnotation);
    }
  }

  render() {
    const { showAnnotation, navigation } = this.props;

    if (this.state.load) {
      return <Load />;
    }

    const idAnnotation = navigation.getParam("idAnnotation", "annotationId");

    return (
      <View style={BaseStyle.container}>
        <View style={BaseStyle.header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <View
              style={{
                justifyContent: "flex-start",
                alignItems: "flex-start",
                marginTop: 35,
                marginLeft: 20
              }}
            >
              <Ionicons
                name={
                  Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"
                }
                size={Platform.OS === "ios" ? 40 : 25}
                color="#4a494a"
              />
            </View>
          </TouchableOpacity>
          <Text style={BaseStyle.textHeader}>Alvarás Control</Text>
          <View style={BaseStyle.trashContainer}>
            <TouchableOpacity onPress={() => this.askAnnotation()}>
              <EvilIcons name="trash" size={40} color="#4a494a" />
            </TouchableOpacity>
          </View>
        </View>

        {/* Annotation Description */}
        <OffLine>
          <ScrollView>
            <View style={{ marginTop: 15 }}>
              <View style={BaseStyle.titleContainer}>
                <Text
                  style={[
                    BaseStyle.titleInformation,
                    { marginRight: 30, marginLeft: 50 }
                  ]}
                >
                  Minha Anotação
                </Text>
              </View>
              <View style={BaseStyle.listContainer}>
                <Text style={[BaseStyle.FontInfo, { marginLeft: 10 }]}>
                  Descrição:
                </Text>
                <Text
                  numberOfLines={5}
                  ellipsizeMode="tail"
                  style={BaseStyle.infoClient}
                >
                  {showAnnotation.description}
                </Text>
              </View>
            </View>
          </ScrollView>
        </OffLine>
        <TouchableOpacity
          style={BaseStyle.circleButton}
          onPress={() =>
            this.props.navigation.navigate("StackEditAnnotion", {
              idAnnotation,
              annotation: showAnnotation.description
            })
          }
        >
          <View style={BaseStyle.circleButtonContainer}>
            <MaterialCommunityIcons name="pencil" size={18} color="#FFFFFF" />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { showAnnotation, annotations } = state.annotation;
  const { isConnect } = state.setNetInfo;
  return {
    showAnnotation: showAnnotation,
    isConnect,
    annotations
  };
};

const MapDispatchToProps = dispatch => {
  return {
    loadAnnotation: id => dispatch(getAnnotation(id)),
    deleteAnnotation: id => dispatch(removeAnnotation(id))
  };
};

export default connect(
  mapStateToProps,
  MapDispatchToProps
)(InfoAnnotationScreen);
