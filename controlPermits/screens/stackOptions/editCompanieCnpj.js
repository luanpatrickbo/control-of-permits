import React, { PureComponent } from "react";
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { TextInputMask } from "react-native-masked-text";
import { BaseStyle } from "../../styles/base";
import Api from "../../utils/api";
import { setValue, clearValues } from "../../actions/valuesActions";
import { editCompanie } from "../../actions/clientActions";
import TitleRegister from "../components/TitleRegister";
import ArrowBackwithExit from "../components/ArrowBackwithExit";
import OffLineForm from "../components/OffLineForms";

class EditCompanieCnpj extends PureComponent {
  state = {
    loading: false,
    cnpj: "",
    responsibleName: "",
    check_cnpj: false
  };

  componentDidMount() {
    const cnpj = this.props.navigation.getParam("cnpj", "cnpj-companie");
    const responsibleName = this.props.navigation.getParam(
      "responsible_name",
      "client-responsibleName"
    );
    this.setState({ cnpj, responsibleName });
  }

  checkCnpj = async () => {
    const { isConnect, navigation, editCompanie } = this.props;

    const { cnpj, responsibleName } = this.state;
    const id = navigation.getParam("id", "client-id");
    const socialName = navigation.getParam("socialName", "socialName-companie");
    const fancyName = navigation.getParam("fancyName", "fancyName-companie");
    const cnpjCompanie = navigation.getParam("cnpj", "cnpj-companie");

    this.setState({ loading: true });

    try {
      if (isConnect) {
        let result;
        if (cnpjCompanie !== cnpj.replace(/\D/g, "")) {
          result = await Api.checkCnpj(cnpj.replace(/\D/g, ""));
          this.setState({ check_cnpj: result });
          console.log(result);
        }

        if (!result) {
          await editCompanie(id, {
            fancyName,
            socialName,
            responsibleName,
            cnpj
          });
          this.setState({ loading: false });
          navigation.navigate("StackInfoClient");
        }
      }
      this.setState({ loading: false });
    } catch (error) {
      alert("tente novamente mais tarde");
      this.setState({ loading: false });
    }
  };

  render() {
    const { cnpj, responsibleName, check_cnpj, loading } = this.state;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackInfoClient"
        />
        <TitleRegister title={"Agora o CNPJ e nome do responsavel"} />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {check_cnpj ? (
            <View style={BaseStyle.textValidationContainer}>
              <Text style={BaseStyle.textValidation}>CNPJ já cadastrado</Text>
            </View>
          ) : null}
          <TextInputMask
            style={BaseStyle.input}
            value={cnpj}
            refInput={ref => {
              this.input = ref;
            }}
            onChangeText={value => this.setState({ cnpj: value })}
            placeholder={`CNPJ (somente números)`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            type={"cnpj"}
            maxLength={18}
            keyboardType={"numeric"}
          />

          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            value={responsibleName}
            onChangeText={value => this.setState({ responsibleName: value })}
            placeholder="Nome do responsavel"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={40}
          />
          <View style={BaseStyle.buttonContainer}>
            {cnpj.trim().replace(/\D/g, "").length === 14 &&
            responsibleName.trim().length >= 4 ? (
              <TouchableOpacity onPress={() => this.checkCnpj()}>
                {loading ? (
                  <ActivityIndicator size={40} color="#EE6E73" />
                ) : (
                  <Text style={BaseStyle.button}>Editar</Text>
                )}
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Editar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForm />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { cnpj, responsibleName, socialName, fancyName } = state.register;
  const { isConnect } = state.setNetInfo;

  return {
    cnpj: cnpj,
    responsibleName: responsibleName,
    socialName: socialName,
    fancyName: fancyName,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues()),
    editCompanie: (id, values) => dispatch(editCompanie(id, values))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditCompanieCnpj);
