import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { setValue, clearValues } from "../../actions/valuesActions";
import { editAnnotation } from "../../actions/annotationActions";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "../components/TitleRegister";
import ArrowBack from "../components/ArrowBack";
import OffLineForms from "../components/OffLineForms";

class AnnotationForm extends Component {
  state = {
    annotationValidation: false,
    annotation: ""
  };

  // onValidationAnnotation = value => {
  //   this.props.setValue("annotation", value);
  //   const express = /[a-zA-Z0-9]{1,150}/;
  //   const testExpress = new RegExp(express);
  //   const test = testExpress.test(value);

  //   if (test) {
  //     this.setState({ annotationValidation: true });
  //   } else {
  //     this.setState({ annotationValidation: false });
  //   }
  // };

  componentDidMount() {
    const annotation = this.props.navigation.getParam(
      "annotation",
      "annotationParams"
    );
    this.setState({ annotation });
  }

  alterAnnotation = async () => {
    const { navigation, isConnect, editAnnotation, clearValues } = this.props;
    const idAnnotation = navigation.getParam("idAnnotation", "annotationId");
    const { annotation } = this.state;
    try {
      if (isConnect) {
        editAnnotation(idAnnotation, annotation);
        await clearValues();
        navigation.goBack();
      }
    } catch (error) {
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { annotationValidation } = this.state;
    const originalAnnotation = this.props.navigation.getParam(
      "annotation",
      "annotationParams"
    );

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Edite a anotação" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            placeholder="Descrição"
            placeholderTextColor={BaseStyle.placeholderStyle}
            style={BaseStyle.input}
            underlineColorAndroid="transparent"
            multiline={true}
            numberOfLines={4}
            autoCorrect={false}
            maxLength={150}
            value={this.state.annotation}
            onChangeText={value => this.setState({ annotation: value })}
          />
          <View style={BaseStyle.buttonContainer}>
            {this.state.annotation.trim() !== originalAnnotation.trim() &&
            this.state.annotation.trim() !== 2 ? (
              <TouchableOpacity onPress={() => this.alterAnnotation()}>
                <Text style={BaseStyle.button}>Editar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Editar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { annotation } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    annotation,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    editAnnotation: (id, value) => dispatch(editAnnotation(id, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnnotationForm);
