import React, { Component } from "react";
import {
  View,
  Text,
  Picker,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import { BaseStyle } from "../../styles/base";
import ArrowBackwithExit from "../components/ArrowBackwithExit";
import TitleRegister from "../components/TitleRegister";
import OffLineForms from "../components/OffLineForms";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getAllKinds } from "../../actions/kindActions";
import { clearValues, setValue } from "../../actions/valuesActions";
import { editPermits } from "../../actions/clientActions";

// PICKER BUG

class EditPermits2 extends Component {
  state = {
    kinds: "",
    kindId: 0,
    isVisible: false,
    date1: new Date()
  };

  componentDidMount() {
    this.props.loadKinds();
    const shelfLife2 = this.props.navigation.getParam(
      "shelfLife2",
      "shelife-dates"
    );
    const test = moment(shelfLife2[1]).format("MM/DD/YYYY");
    const fireDepartment = new Date(test);
    this.setState({ date1: fireDepartment });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.kinds !== this.props.kinds) {
      const selectedValue = this.props.kinds[1].id;
      this.props.setValue("kind1", selectedValue);
    }
  }

  handlePicker = date1 => {
    this.setState({
      isVisible: false
    });
    this.setState({ date1 });
  };

  hidePicker = () => {
    this.setState({ isVisible: false });
  };

  showPicker = () => {
    this.setState({ isVisible: true });
  };

  editPermit = async () => {
    const {
      navigation,
      editPermits,
      kind,
      kind1,
      isConnect,
      clearValues
    } = this.props;
    const { date1 } = this.state;
    const permitsId = navigation.getParam("permitsId", "permits-id");
    const id = navigation.getParam("id", "client-id");
    const date = navigation.getParam("date", "date-client");

    try {
      if (isConnect) {
        editPermits(id, permitsId, {
          date,
          kind,
          kind1,
          date1
        });
      }
      await clearValues();
      navigation.navigate("StackInfoClient");
    } catch (error) {
      console.log(error);
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { isVisible } = this.state;
    const maximumDate = new Date();
    maximumDate.setFullYear(maximumDate.getFullYear() + 2);
    const minimumDate = new Date();
    minimumDate.setMonth(minimumDate.getMonth() - 6);

    // Origin attributes

    const date = this.props.navigation.getParam("date", "date-client");
    const shelfLife2 = this.props.navigation.getParam(
      "shelfLife2",
      "shelife-dates"
    );

    const date0 = moment(date).format("YYYY-MM-DD");
    const date1 = moment(this.state.date1).format("YYYY-MM-DD");
    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackInfoClient"
        />
        <TitleRegister title="Edite a data de validade do Alvará de Licença do Município" />
        <View style={BaseStyle.formContainer}>
          <View
            style={[
              BaseStyle.pickerContainer,
              { marginBottom: 20, marginLeft: 20, marginRight: 20 }
            ]}
          >
            <Picker
              style={{ padding: 10, backgroundColor: "#F7F7F7" }}
              onValueChange={value => this.props.setValue("kind1", value)}
              selectedValue={this.props.kind1}
            >
              {this.props.kinds.map(kind => (
                <Picker.Item key={kind.id} label={kind.kind} value={kind.id} />
              ))}
            </Picker>
          </View>
          <TouchableWithoutFeedback onPress={() => this.showPicker()}>
            <View>
              <Text style={BaseStyle.datePicker}>
                {moment(this.state.date1).format("DD/MM/YYYY")}
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <DateTimePicker
            isVisible={isVisible}
            onConfirm={this.handlePicker}
            onCancel={this.hidePicker}
            minimumDate={minimumDate}
            maximumDate={maximumDate}
            date={this.state.date1}
          />
          <View style={BaseStyle.buttonContainer}>
            <TouchableOpacity
              disabled={this.props.kind !== this.props.kind1 ? false : true}
              onPress={() => this.editPermit()}
            >
              <Text
                style={
                  this.props.kind !== this.props.kind1
                    ? BaseStyle.button
                    : [BaseStyle.button, { backgroundColor: "#D46266" }]
                }
              >
                Editar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <OffLineForms />
      </View>
    );
  }
}

EditPermits2.propTypes = {
  navigation: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { kind, date, date1, kind1 } = state.register;

  const { isConnect } = state.setNetInfo;

  return {
    kinds: state.kind.kinds,
    kind: kind,
    date: date,
    date1: date1,
    kind1: kind1,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadKinds: () => dispatch(getAllKinds()),
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues()),
    editPermits: (id, permitIds, values) => {
      dispatch(editPermits(id, permitIds, values));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPermits2);
