import React, { Component } from "react";
import { View } from "react-native";
import { BaseStyle } from "../../styles/base";
import SelectState2 from "../components/SelectState2";
import TitleRegister from "../components/TitleRegister";
import ArrowBackwithExit from "../components/ArrowBackwithExit";
import OffLineForms from "../components/OffLineForms";

class StateScreen extends Component {
  render() {
    const state = this.props.navigation.getParam("state", "state-companie");
    const EditState = this.props.navigation.getParam(
      "EditState",
      "editstate-companie"
    );

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackInfoClient"
        />
        <TitleRegister title="Estamos Quase Lá !! Agora Informe o estado" />
        <SelectState2
          navigation={this.props.navigation}
          state={state}
          EditState={EditState}
        />
        <OffLineForms />
      </View>
    );
  }
}

export default StateScreen;
