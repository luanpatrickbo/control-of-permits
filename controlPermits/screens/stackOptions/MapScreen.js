import React, { Component } from "react";
import { View, Text } from "react-native";
import { MapView } from "expo";
import { connect } from "react-redux";
import { getAllGeocoding } from "../../actions/geocodingAction";
import Load from "../components/Loading";
import ArrowBack from "../components/ArrowBack";
import { BaseStyle } from "../../styles/base";
import { Feather } from "@expo/vector-icons";

class MapScreen extends Component {
  async componentDidMount() {
    // Get client address
    const { navigation } = this.props;
    const address = navigation.getParam("address", "addressDescription");
    const number = navigation.getParam("number", "addressNumber");
    const city = navigation.getParam("city", "addressCity");
    const state = navigation.getParam("state", "addressState");

    await this.props.loadgeocoding(address, number, city, state);

    this.setState({ loading: false });
  }

  state = {
    loading: true
  };

  render() {
    if (this.state.loading) {
      return <Load />;
    }

    const { results } = this.props;
    return (
      <View style={BaseStyle.mapContainer}>
        <ArrowBack navigation={this.props.navigation} />
        {results.status === "OK" ? (
          results.results.map(result => (
            <MapView
              key={result.place_id}
              style={BaseStyle.maps}
              initialRegion={{
                latitude: result.geometry.location.lat,
                longitude: result.geometry.location.lng,
                latitudeDelta: 0.0043,
                longitudeDelta: 0.0034
              }}
            >
              <MapView.Marker
                coordinate={{
                  latitude: result.geometry.location.lat,
                  longitude: result.geometry.location.lng
                }}
              />
            </MapView>
          ))
        ) : (
          <View style={BaseStyle.warningContainer}>
            <Feather name="alert-triangle" size={90} color="#4a494a" />
            <Text style={BaseStyle.warningText}>
              Não foi possivel encontrar seu endereço
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { results } = state.geocoding;
  return {
    results: results
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadgeocoding: (address, number, city, state) =>
      dispatch(getAllGeocoding(address, number, city, state))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapScreen);
