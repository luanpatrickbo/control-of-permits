import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { EvilIcons } from "@expo/vector-icons";
import { BaseStyle } from "../../styles/base";
import { Ionicons, Foundation } from "@expo/vector-icons";
import { showClient, deleteClient } from "../../actions/clientActions";
import { setValue } from "../../actions/valuesActions";
import Load from "../components/Loading";
import moment from "moment";
import OffLine from "../components/OffLine";

class infoClientScreen extends Component {
  state = {
    loading: true
  };

  async componentDidMount() {
    const { navigation, loadClient, isConnect, client, setValue } = this.props;
    let id = navigation.getParam("id", "clientId");
    if (isConnect) {
      await loadClient(id);
    }
    this.setState({ loading: false });
  }

  componentDidUpdate(prevProps) {
    const { navigation, loadClient, isConnect } = this.props;
    const id = navigation.getParam("id", "clientId");
    if (
      JSON.stringify(prevProps.clients) !== JSON.stringify(this.props.clients)
    ) {
      if (isConnect) {
        loadClient(id);
      }
    }
  }

  askDeleteClient = () => {
    Alert.alert(
      "Você Realmente deseja excluir este cliente?",
      "Todos os dados do cliente serão perdidos",
      [
        {
          text: "Cancelar",
          style: "cancel"
        },
        { text: "Sim, desejo excluir", onPress: () => this.onDeleteClient() }
      ],
      { cancelable: false }
    );
  };

  onDeleteClient = () => {
    const { navigation, deleteClient } = this.props;
    const id = navigation.getParam("id", "client-id");
    deleteClient(id);

    navigation.goBack();
  };

  render() {
    if (this.state.loading) {
      return <Load />;
    }

    const { client, navigation, isConnect } = this.props;

    const phones = client.phones.slice();

    const EditAdress = client.address.address_description;
    const EditNeighborhood = client.address.neighborhood;
    const EditNumber = client.address.number;
    const EditCity = client.address.city;
    const Editstate = client.address.state;
    const EditCep = client.address.cep;

    return (
      <View style={BaseStyle.container}>
        <View style={BaseStyle.header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <View
              style={{
                justifyContent: "flex-start",
                alignItems: "flex-start",
                marginTop: 35,
                marginLeft: 20
              }}
            >
              <Ionicons
                name={
                  Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"
                }
                size={Platform.OS === "ios" ? 40 : 25}
                color="#4a494a"
              />
            </View>
          </TouchableOpacity>
          <Text style={BaseStyle.textHeader}>Alvarás Control</Text>
          <View style={BaseStyle.trashContainer}>
            <TouchableOpacity
              onPress={() => this.askDeleteClient()}
              disabled={!isConnect ? true : false}
            >
              <EvilIcons name="trash" size={40} color="#4a494a" />
            </TouchableOpacity>
          </View>
        </View>
        <OffLine>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* information */}

            <View style={[BaseStyle.section, { marginTop: 15 }]}>
              <View style={BaseStyle.titleContainer}>
                <Text
                  style={[
                    BaseStyle.titleInformation,
                    { marginRight: 30, marginLeft: 50 }
                  ]}
                >
                  Informações da empresa
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("StackEditCompanie", {
                      id: client.id,
                      social_name: client.social_name,
                      fancy_name: client.fancy_name,
                      cnpj: client.cnpj,
                      responsible_name: client.responsible_name
                    })
                  }
                >
                  <Foundation
                    name="pencil"
                    size={28}
                    color="#4a494a"
                    style={{ marginRight: 20 }}
                  />
                </TouchableOpacity>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Razão Social:</Text>
                <Text style={BaseStyle.info}>{client.social_name}</Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Nome Fantasia:</Text>
                <Text style={BaseStyle.info}>{client.fancy_name}</Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>CNPJ:</Text>
                <Text style={BaseStyle.info}>
                  {client.cnpj.replace(
                    /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
                    "$1.$2.$3/$4-$5"
                  )}
                </Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Nome do Responsável:</Text>
                <Text style={BaseStyle.info}>{client.responsible_name}</Text>
              </View>
            </View>

            {/* Phones */}

            <View style={BaseStyle.section}>
              <View style={BaseStyle.titleContainer}>
                <Text style={[BaseStyle.titleInformation, { marginLeft: 125 }]}>
                  Telefones
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("StackEditPhones", {
                      ids: client.phones.map(phone => phone.id),
                      numbers: client.phones.map(phone => phone.number),
                      id: client.id
                    })
                  }
                >
                  <Foundation
                    name="pencil"
                    size={28}
                    color="#4a494a"
                    style={{ marginRight: 20 }}
                  />
                </TouchableOpacity>
              </View>

              {phones.map(phone => (
                <View key={phone.id} style={BaseStyle.infoTitleContainer}>
                  <Text style={BaseStyle.infoTitle}>
                    {phone.number.length >= 11 ? "Celular:" : "Telefone:"}
                  </Text>
                  <Text style={BaseStyle.info}>
                    {phone.number.length >= 11
                      ? phone.number.replace(
                        /(\d{2})(\d{5})(\d{4})/,
                        "$1 $2-$3"
                      )
                      : phone.number.replace(
                        /(\d{2})(\d{4})(\d{4})/,
                        "$1 $2-$3"
                      )}
                  </Text>
                </View>
              ))}
            </View>

            {/* Address */}

            <View style={BaseStyle.section}>
              <View style={BaseStyle.titleContainer}>
                <Text style={[BaseStyle.titleInformation, { marginLeft: 125 }]}>
                  Endereço
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("StackEditAdress", {
                      id: client.address.id,
                      address_description: client.address.address_description,
                      neighborhood: client.address.neighborhood,
                      number: client.address.number,
                      city: client.address.city,
                      state: client.address.state,
                      cep: client.address.cep,
                      EditAdress,
                      EditNeighborhood,
                      EditNumber,
                      EditCep,
                      EditCity,
                      Editstate
                    })
                  }
                >
                  <Foundation
                    name="pencil"
                    size={28}
                    color="#4a494a"
                    style={{ marginRight: 20 }}
                  />
                </TouchableOpacity>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Endereço:</Text>
                <Text style={BaseStyle.info}>
                  {client.address.address_description}
                </Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Bairro:</Text>
                <Text style={BaseStyle.info}>
                  {client.address.neighborhood}
                </Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Número:</Text>
                <Text style={BaseStyle.info}>{client.address.number}</Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Cidade:</Text>
                <Text style={BaseStyle.info}>{client.address.city}</Text>

                <View style={BaseStyle.infoMapContainer}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("StackMap", {
                        address: client.address.address_description,
                        neighborhood: client.address.neighborhood,
                        number: client.address.number,
                        city: client.address.city,
                        state: client.address.state
                      })
                    }
                  >
                    <View>
                      <Text
                        style={{
                          color: "#EE6E73",
                          fontSize: 17,
                          fontWeight: "600"
                        }}
                      >
                        Ver no Mapa
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>Estado:</Text>
                <Text style={BaseStyle.info}>{client.address.state}</Text>
              </View>

              <View style={BaseStyle.infoTitleContainer}>
                <Text style={BaseStyle.infoTitle}>CEP:</Text>
                <Text style={BaseStyle.info}>
                  {client.address.cep.replace(/(\d{5})(\d{3})/, "$1-$2")}
                </Text>
              </View>
            </View>

            {/* Permits */}

            <View style={BaseStyle.section}>
              <View style={BaseStyle.titleContainer}>
                <Text style={[BaseStyle.titleInformation, { marginLeft: 135 }]}>
                  Alvarás
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("StackEditPermits", {
                      permitsId: client.permits.map(permit => permit.id),
                      shelfLife: client.permits.map(
                        permit => permit.shelf_life
                      ),
                      id: client.id
                    })
                  }
                >
                  <Foundation
                    name="pencil"
                    size={28}
                    color="#4a494a"
                    style={{ marginRight: 20 }}
                  />
                </TouchableOpacity>
              </View>
              {client.permits.map(permit => (
                <View key={permit.id} style={BaseStyle.infoTitleContainer}>
                  <Text style={BaseStyle.infoTitle}>
                    {permit.kind_id === 1 ? "Corpo de Bombeiros" : "Licença"}:
                  </Text>
                  <Text style={BaseStyle.info}>
                    {moment(permit.shelf_life).format("DD/MM/YYYY")}
                  </Text>
                </View>
              ))}
            </View>
          </ScrollView>
        </OffLine>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  const { clients } = state.client;

  return {
    client: state.client.showClient,
    isConnect,
    clients
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadClient: id => dispatch(showClient(id)),
    deleteClient: id => dispatch(deleteClient(id)),
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(infoClientScreen);
