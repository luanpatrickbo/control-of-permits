import React, { Component } from "react";
import {
  View,
  Text,
  Picker,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import { BaseStyle } from "../../styles/base";
import ArrowBack from "../components/ArrowBack";
import TitleRegister from "../components/TitleRegister";
import OffLineForms from "../components/OffLineForms";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getAllKinds } from "../../actions/kindActions";
import { clearValues, setValue } from "../../actions/valuesActions";

// PICKER BUG

class TypeOfLicenses extends Component {
  state = {
    kinds: "",
    kindId: 0,
    isVisible: false,
    date: new Date()
  };

  componentDidMount() {
    this.props.loadKinds();
    const shelfLife = this.props.navigation.getParam(
      "shelfLife",
      "shelife-dates"
    );
    const test = moment(shelfLife[0]).format("MM/DD/YYYY");
    const fireDepartment = new Date(test);

    this.setState({ date: fireDepartment });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.kinds !== this.props.kinds) {
      const selectedValue = this.props.kinds[0].id;
      this.props.setValue("kind", selectedValue);
    }
  }

  handlePicker = date => {
    this.setState({
      isVisible: false
    });
    this.setState({ date });
  };

  hidePicker = () => {
    this.setState({ isVisible: false });
  };

  showPicker = () => {
    this.setState({ isVisible: true });
  };

  render() {
    const { isVisible } = this.state;

    const permitsId = this.props.navigation.getParam("permitsId", "permits-id");
    const id = this.props.navigation.getParam("id", "client-id");
    const shelfLife = this.props.navigation.getParam(
      "shelfLife",
      "shelife-dates"
    );
    const maximumDate = new Date();
    maximumDate.setFullYear(maximumDate.getFullYear() + 2);
    const minimumDate = new Date();
    minimumDate.setMonth(minimumDate.getMonth() - 6);

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Edite a data de validade do Certificado de Corpo de Bombeiros" />
        <View style={BaseStyle.formContainer}>
          <View
            style={[
              BaseStyle.pickerContainer,
              { marginBottom: 20, marginLeft: 20, marginRight: 20 }
            ]}
          >
            <Picker
              style={{ padding: 10, backgroundColor: "#F7F7F7" }}
              onValueChange={value => this.props.setValue("kind", value)}
              selectedValue={this.props.kind}
            >
              {this.props.kinds.map(kind => (
                <Picker.Item key={kind.id} label={kind.kind} value={kind.id} />
              ))}
            </Picker>
          </View>
          <TouchableWithoutFeedback onPress={() => this.showPicker()}>
            <View>
              <Text style={BaseStyle.datePicker}>
                {moment(this.state.date).format("DD/MM/YYYY")}
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <DateTimePicker
            isVisible={isVisible}
            onConfirm={this.handlePicker}
            onCancel={this.hidePicker}
            minimumDate={minimumDate}
            maximumDate={maximumDate}
            date={this.state.date}
          />
          <View style={BaseStyle.buttonContainer}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("StackEditPermits2", {
                  permitsId,
                  id,
                  date: this.state.date,
                  shelfLife2: shelfLife
                })
              }
            >
              <Text style={BaseStyle.button}>Avançar</Text>
            </TouchableOpacity>
          </View>
        </View>
        <OffLineForms />
      </View>
    );
  }
}

TypeOfLicenses.propTypes = {
  navigation: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { kind, date, date1, kind1, editDate2 } = state.register;

  const { isConnect } = state.setNetInfo;

  return {
    kinds: state.kind.kinds,
    kind: kind,
    date: date,
    date1: date1,
    kind1: kind1,
    isConnect: isConnect,
    editDate2
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadKinds: () => dispatch(getAllKinds()),
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TypeOfLicenses);
