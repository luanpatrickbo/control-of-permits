import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { connect } from "react-redux";
import { setValue } from "../../actions/valuesActions";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "../components/TitleRegister";
import ArrowBackwithExit from "../components/ArrowBackwithExit";
import OffLineForms from "../components/OffLineForms";

class EditCep extends Component {
  state = {
    cep_validation: false,
    city_validation: false,
    cep: "",
    number: ""
  };

  componentDidMount() {
    const cep = this.props.navigation.getParam("cep", "cep-companie");
    const number = this.props.navigation.getParam("number", "number-companie");
    this.setState({ cep, number });
  }

  // Present Validation

  onValidatonCep = value => {
    this.props.setValue("cep", value);

    value = value.replace(/\D/g, "");

    const expression = /[0-9]{8,9}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ cep_validation: true });
    } else {
      this.setState({ cep_validation: false });
    }
  };

  onValidationCity = value => {
    this.props.setValue("number", value);

    const expression = /[0-9]{2,4}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ city_validation: true });
    } else {
      this.setState({ city_validation: false });
    }
  };

  render() {
    const { cep, number } = this.state;
    const { navigation } = this.props;

    const id = navigation.getParam("id", "adress-id");
    const adress = navigation.getParam("adress", "adress-companie");
    const state = navigation.getParam("state", "state-companie");
    const city = navigation.getParam("city", "city-componie");
    const neighborhood = navigation.getParam(
      "neighborhood",
      "neighborhood-companie"
    );

    // Original Atributes

    const EditAdress = navigation.getParam("EditAdress", "EditAdress-companie");
    const EditNumber = navigation.getParam("EditNumber", "EditNumber-companie");
    const EditCep = navigation.getParam("EditCep", "EditCep-companie");
    const EditState = navigation.getParam("EditState", "EditState-comapanie");
    const EditCity = navigation.getParam("EditCity", "EditCity-companie");
    const EditNeighborhood = navigation.getParam(
      "EditNeighborhood",
      "EditNeighborhood-companie"
    );

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackInfoClient"
        />
        <TitleRegister title="Edite o CEP da empresa e a cidade" />
        <KeyboardAvoidingView
          style={BaseStyle.formContainer}
          behavior="padding"
        >
          <TextInput
            style={[BaseStyle.input]}
            placeholder="Número"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            maxLength={4}
            autoCorrect={false}
            value={number}
            keyboardType="numeric"
            onChangeText={value => this.setState({ number: value })}
          />

          <TextInputMask
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder={`CEP (Somente números)`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            maxLength={9}
            autoCorrect={false}
            value={cep}
            keyboardType={"numeric"}
            onChangeText={value => this.setState({ cep: value })}
            type={"zip-code"}
          />

          <View style={BaseStyle.buttonContainer}>
            {cep.replace(/\D/g, "").trim().length >= 8 &&
            number.trim().length >= 2 ? (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("StackEditState", {
                    id,
                    adress,
                    neighborhood,
                    city,
                    state,
                    number,
                    cep,
                    EditAdress,
                    EditNeighborhood,
                    EditNumber,
                    EditCep,
                    EditCity,
                    EditState
                  })
                }
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cep: state.register.cep,
    number: state.register.number
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditCep);
