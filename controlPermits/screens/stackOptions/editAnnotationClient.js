import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { setValue, clearValues } from "../../actions/valuesActions";
import { editAnnotationClient } from "../../actions/clientActions";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "../components/TitleRegister";
import ArrowBack from "../components/ArrowBack";
import OffLineForms from "../components/OffLineForms";

class EditAnnotationClient extends Component {
  state = {
    annotationValidation: false
  };

  onValidationAnnotation = value => {
    this.props.setValue("annotation", value);
    const express = /[a-zA-Z0-9]{1,150}/;
    const testExpress = new RegExp(express);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ annotationValidation: true });
    } else {
      this.setState({ annotationValidation: false });
    }
  };

  alterAnnotationClient = async () => {
    const {
      navigation,
      annotation,
      isConnect,
      clearValues,
      editAnnotationClient
    } = this.props;

    const annotationId = navigation.getParam("annotationId", "id-annotation");
    const id = navigation.getParam("id", "client-id");

    try {
      if (isConnect) {
        editAnnotationClient(id, annotationId, {
          annotation
        });
        await clearValues();
        navigation.goBack();
      }
    } catch (error) {
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { annotationValidation } = this.state;

    // const annotationId = this.props.navigation.getParam(
    //   "annotationId",
    //   "id-annotation"
    // );
    // console.log(annotationId);

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Edite a anotação do cliente" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            placeholder="Descrição"
            placeholderTextColor={BaseStyle.placeholderStyle}
            style={BaseStyle.input}
            underlineColorAndroid="transparent"
            multiline={true}
            numberOfLines={4}
            autoCorrect={false}
            maxLength={150}
            value={this.props.annotation}
            onChangeText={value => this.onValidationAnnotation(value)}
          />
          <View style={BaseStyle.buttonContainer}>
            {annotationValidation ? (
              <TouchableOpacity onPress={() => this.alterAnnotationClient()}>
                <Text style={BaseStyle.button}>Editar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Editar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  const { annotation } = state.register;

  return {
    annotation,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues()),
    editAnnotationClient: (id, annotationId, values) => {
      dispatch(editAnnotationClient(id, annotationId, values));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditAnnotationClient);
