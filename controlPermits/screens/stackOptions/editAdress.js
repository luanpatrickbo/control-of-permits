import React, { PureComponent } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { BaseStyle } from "../../styles/base";
import TitleRegister from "../components/TitleRegister";
import ArrowBack from "../components/ArrowBack";
import OffLineForms from "../components/OffLineForms";

class EditAdress extends PureComponent {
  state = {
    adress_validation: false,
    number_validation: false,
    adress: "",
    neighborhood: ""
  };

  componentDidMount() {
    const neighborhood = this.props.navigation.getParam(
      "neighborhood",
      "neighborhood-client"
    );
    const address_description = this.props.navigation.getParam(
      "address_description",
      "address_description-client"
    );

    this.setState({ adress: address_description });
    this.setState({ neighborhood: neighborhood });
  }

  render() {
    const { neighborhood, adress } = this.state;
    const { navigation } = this.props;

    const id = navigation.getParam("id", "adress-id");
    const city = navigation.getParam("city", "city-componie");
    const state = navigation.getParam("state", "state-componie");
    const cep = navigation.getParam("cep", "cep-companie");
    const number = navigation.getParam("number", "number-companie");

    // ORIGINAL ATRIBUTES

    const EditAdress = navigation.getParam("EditAdress", "EditAdress-companie");
    const EditNumber = navigation.getParam("EditNumber", "EditNumber-companie");
    const EditCep = navigation.getParam("EditCep", "EditCep-companie");
    const EditCity = navigation.getParam("EditCity", "EditCity-companie");
    const EditState = navigation.getParam("Editstate", "Editstate-companie");
    const EditNeighborhood = navigation.getParam(
      "EditNeighborhood",
      "EditNeighborhood-companie"
    );

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title={"Edite o endereço da empresa"} />
        <KeyboardAvoidingView
          style={BaseStyle.formContainer}
          behavior="padding"
        >
          <TextInput
            style={BaseStyle.input}
            placeholder="Endereço"
            placeholderTextColor={BaseStyle.placeholderStyle}
            autoCorrect={false}
            underlineColorAndroid="transparent"
            maxLength={80}
            value={adress}
            onChangeText={value => this.setState({ adress: value })}
          />

          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder="Bairro"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            maxLength={60}
            autoCorrect={false}
            value={neighborhood}
            onChangeText={value => this.setState({ neighborhood: value })}
          />

          <View style={BaseStyle.buttonContainer}>
            {adress.trim().length !== 0 && neighborhood.trim().length >= 2 ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("StackEditCep", {
                    id,
                    adress,
                    neighborhood,
                    city,
                    number,
                    state,
                    cep,
                    EditAdress,
                    EditNeighborhood,
                    EditNumber,
                    EditCep,
                    EditCity,
                    EditState
                  });
                }}
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

export default EditAdress;
