import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { BaseStyle } from "../../../styles/base";
import TitleRegister from "../../components/TitleRegister";
import AnnotationForm from "../../components/AnnotationForm";
import ArrowBack from "../../components/ArrowBack";
import OffLineForms from "../../components/OffLineForms";

class Annotation2Screen extends Component {
  state = {
    permit: false
  };

  render() {
    if (this.state.permit) {
      return <AnnotationForm navigation={this.props.navigation} />;
    }

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title="Deseja adicionar uma anotação para este cliente ?" />
        <View style={BaseStyle.askContainer}>
          <TouchableOpacity onPress={() => this.setState({ permit: true })}>
            <Text style={[BaseStyle.button, { marginRight: 16 }]}>Sim</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("StackPermit")}
          >
            <Text style={BaseStyle.button}>Não</Text>
          </TouchableOpacity>
        </View>
        <OffLineForms />
      </View>
    );
  }
}

export default Annotation2Screen;
