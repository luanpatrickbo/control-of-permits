import React, { Component } from "react";
import {
  View,
  Text,
  Picker,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import { BaseStyle } from "../../../styles/base";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import TitleRegister from "../../components/TitleRegister";
import OffLineForms from "../../components/OffLineForms";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { postClient } from "../../../actions/clientActions";
import { getAllKinds } from "../../../actions/kindActions";
import { clearValues, setValue } from "../../../actions/valuesActions";

class TypeOfLicenses extends Component {
  state = {
    kinds: "",
    date1: new Date(),
    kindId: 0,
    isVisible: false
  };

  componentDidMount() {
    this.props.loadKinds();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.kinds !== this.props.kinds) {
      const selectedValue = this.props.kinds[1].id;
      this.props.setValue("kind1", selectedValue);
    }
  }

  handlePicker = date1 => {
    this.setState({
      isVisible: false,
      date1
    });
  };

  hidePicker = () => {
    this.setState({ isVisible: false });
  };

  showPicker = () => {
    this.setState({ isVisible: true });
  };

  CreateClient = async () => {
    const {
      socialName,
      fancyName,
      cnpj,
      responsibleName,
      adress,
      number,
      cep,
      city,
      states,
      phone,
      cellPhone,
      kind,
      kind1,
      neighborhood,
      isConnect
    } = this.props;
    const { date1 } = this.state;
    const date = this.props.navigation.getParam("date", "date-typeofScreen");

    if (isConnect) {
      this.props.createClient({
        socialName: socialName,
        fancyName: fancyName,
        cnpj: cnpj,
        responsibleName: responsibleName,
        adress: adress,
        number: number,
        cep: cep,
        city: city,
        neighborhood: neighborhood,
        states: states,
        phone: phone,
        cellPhone: cellPhone,
        kind: kind,
        date: date,
        date1: date1,
        kind1: kind1
      });
      await this.props.clearValues();
      this.props.navigation.navigate("mainScreen");
    }
  };

  render() {
    const { isVisible } = this.state;
    const maximumDate = new Date();
    maximumDate.setFullYear(maximumDate.getFullYear() + 2);
    const minimumDate = new Date();
    minimumDate.setMonth(minimumDate.getMonth() - 6);

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title="Informe a data de validade do Alvará de Licença do Municipío" />
        <View style={BaseStyle.formContainer}>
          <View
            style={[
              BaseStyle.pickerContainer,
              { marginBottom: 20, marginLeft: 20, marginRight: 20 }
            ]}
          >
            <Picker
              style={{ padding: 10, backgroundColor: "#F7F7F7" }}
              onValueChange={value => this.props.setValue("kind1", value)}
              selectedValue={this.props.kind1}
            >
              {this.props.kinds.map(kind => (
                <Picker.Item key={kind.id} label={kind.kind} value={kind.id} />
              ))}
            </Picker>
          </View>
          <TouchableWithoutFeedback onPress={() => this.showPicker()}>
            <View>
              <Text style={BaseStyle.datePicker}>
                {moment(this.state.date1).format("DD/MM/YYYY")}
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <DateTimePicker
            isVisible={isVisible}
            onConfirm={this.handlePicker}
            onCancel={this.hidePicker}
            maximumDate={maximumDate}
            minimumDate={minimumDate}
            date={this.state.date1}
          />
          <View style={BaseStyle.buttonContainer}>
            <TouchableOpacity
              disabled={this.props.kind !== this.props.kind1 ? false : true}
              onPress={() => this.CreateClient()}
            >
              <Text
                style={
                  this.props.kind !== this.props.kind1
                    ? BaseStyle.button
                    : [BaseStyle.button, { backgroundColor: "#D46266" }]
                }
              >
                Cadastrar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <OffLineForms />
      </View>
    );
  }
}

TypeOfLicenses.propTypes = {
  navigation: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const {
    socialName,
    fancyName,
    cnpj,
    responsibleName,
    adress,
    number,
    cep,
    city,
    states,
    phone,
    cellPhone,
    kind,
    kind1,
    neighborhood
  } = state.register;

  const { isConnect } = state.setNetInfo;

  return {
    kinds: state.kind.kinds,
    socialName: socialName,
    fancyName: fancyName,
    cnpj: cnpj,
    responsibleName: responsibleName,
    adress: adress,
    number: number,
    cep: cep,
    city: city,
    states: states,
    phone: phone,
    cellPhone: cellPhone,
    kind: kind,
    kind1: kind1,
    neighborhood: neighborhood,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadKinds: () => dispatch(getAllKinds()),
    createClient: values => dispatch(postClient(values)),
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TypeOfLicenses);
