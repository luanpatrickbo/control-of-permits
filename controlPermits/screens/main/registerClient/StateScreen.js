import React, { Component } from "react";
import { View } from "react-native";
import { BaseStyle } from "../../../styles/base";
import SelectState from "../../components/SelectState";
import TitleRegister from "../../components/TitleRegister";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForms from "../../components/OffLineForms";

class StateScreen extends Component {
  render() {
    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title="Estamos quase lá!! agora informe a cidade e estado" />
        <SelectState navigation={this.props.navigation} />
        <OffLineForms />
      </View>
    );
  }
}

export default StateScreen;
