import React, { PureComponent } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { setValue } from "../../../actions/valuesActions";
import { BaseStyle } from "../../../styles/base";
import TitleRegister from "../../components/TitleRegister";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForms from "../../components/OffLineForms";

class AdressScreen extends PureComponent {
  state = {
    adress_validation: false,
    neighborhood_validation: false
  };

  // validate present

  onValidationAdress = value => {
    this.props.setValue("adress", value);

    const expression = /[a-zA-Z0-9]{1,80}/;
    const testExpress = new RegExp(expression);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ adress_validation: true });
    } else {
      this.setState({ adress_validation: false });
    }
  };

  onValidationNeighborhood = value => {
    this.props.setValue("neighborhood", value);

    const expression = /[a-zA-Z]{4,60}/;
    const testExpress = new RegExp(expression);
    const test = testExpress.test(value);

    if (test) {
      this.setState({ neighborhood_validation: true });
    } else {
      this.setState({ neighborhood_validation: false });
    }
  };

  render() {
    const { neighborhood_validation, adress_validation } = this.state;
    const { adress, neighborhood } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title={"Informe o endereço da empresa"} />
        <KeyboardAvoidingView
          style={BaseStyle.formContainer}
          behavior="padding"
        >
          <TextInput
            style={BaseStyle.input}
            placeholder="Endereço"
            placeholderTextColor={BaseStyle.placeholderStyle}
            autoCorrect={false}
            underlineColorAndroid="transparent"
            maxLength={80}
            value={adress}
            onChangeText={value => this.onValidationAdress(value)}
          />

          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder="Bairro"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            maxLength={60}
            autoCorrect={false}
            value={neighborhood}
            onChangeText={value => this.onValidationNeighborhood(value)}
          />
          <View style={BaseStyle.buttonContainer}>
            {adress_validation && neighborhood_validation ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("StackCep");
                }}
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { adress, neighborhood } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    adress,
    neighborhood,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdressScreen);
