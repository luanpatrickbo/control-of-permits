import React, { Component } from "react";
import { View } from "react-native";
import { BaseStyle } from "../../../styles/base";
import TitleRegister from "../../components/TitleRegister";
import TypeOfLicenses from "../../components/TypeOfLicenses";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForms from "../../components/OffLineForms";
class PermitScreen extends Component {
  render() {
    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title="Informe a data de validade do certificado de vistoria do Corpo de Bombeiros" />
        <TypeOfLicenses navigation={this.props.navigation} />
        <OffLineForms />
      </View>
    );
  }
}

export default PermitScreen;
