import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { connect } from "react-redux";
import { setValue } from "../../../actions/valuesActions";
import { BaseStyle } from "../../../styles/base";
import TitleRegister from "../../components/TitleRegister";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForms from "../../components/OffLineForms";
class CepScreen extends Component {
  state = {
    cep_validation: false,
    number_validation: false
  };

  // Present Validation

  onValidatonCep = value => {
    this.props.setValue("cep", value);

    value = value.replace(/\D/g, "");

    const expression = /[0-9]{8,9}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ cep_validation: true });
    } else {
      this.setState({ cep_validation: false });
    }
  };

  onValidationNumber = value => {
    this.props.setValue("number", value);

    const expression = /[0-9]{2,4}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ number_validation: true });
    } else {
      this.setState({ number_validation: false });
    }
  };

  render() {
    const { cep_validation, number_validation } = this.state;
    const { cep, number } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title="Informe o numero da empresa e CEP" />
        <KeyboardAvoidingView
          style={BaseStyle.formContainer}
          behavior="padding"
        >
          <TextInput
            style={BaseStyle.input}
            placeholder="Número"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            maxLength={4}
            autoCorrect={false}
            value={number}
            keyboardType="numeric"
            onChangeText={value => this.onValidationNumber(value)}
          />

          <TextInputMask
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder={`CEP (Somente números)`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            maxLength={9}
            autoCorrect={false}
            value={cep}
            keyboardType="numeric"
            onChangeText={value => this.onValidatonCep(value)}
            type={"zip-code"}
          />

          <View style={BaseStyle.buttonContainer}>
            {cep_validation && number_validation ? (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("StackState")}
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { cep, number } = state.register;
  return {
    cep,
    number
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CepScreen);
