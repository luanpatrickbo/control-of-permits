import React, { PureComponent } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { BaseStyle } from "../../../styles/base";
import { setValue, clearValues } from "../../../actions/valuesActions";
import Api from "../../../utils/api";
import TitleRegister from "../../components/TitleRegister";
import ArrowBack from "../../components/ArrowBack";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForm from "../../components/OffLineForms";

class SocialNameScreen extends PureComponent {
  state = {
    check_social: false,
    check_fancy: false,
    loading: false,
    social_name_validation: false,
    fancy_name_validation: false
  };

  clearValues = async () => {
    const { navigation, clearValues } = this.props;
    await clearValues();
    navigation.goBack();
  };

  checkParams = async () => {
    const { socialName, fancyName, isConnect } = this.props;
    this.setState({ loading: true });

    try {
      if (isConnect) {
        const result = await Promise.all([
          Api.checkSocialName(socialName),
          Api.checkFancyName(fancyName)
        ]);

        this.setState({ check_social: result[0], check_fancy: result[1] });

        if (result[0] === false && result[1] === false) {
          this.props.navigation.navigate("StackCnpj");
        }
      }
      this.setState({ loading: false });
    } catch (error) {
      aler("tente novamente mais tarde");
    }
  };

  // validate present

  onValidationSocialName = val => {
    this.props.setValue("socialName", val);

    const expresion = /[a-zA-Z0-9]{1,50}/;
    const ExpreTest = new RegExp(expresion);
    const test = ExpreTest.test(val);

    if (test) {
      this.setState({ social_name_validation: true });
    } else {
      this.setState({ social_name_validation: false });
    }
  };

  onValidationFancyName = val => {
    this.props.setValue("fancyName", val);

    const expresion = /[a-zA-Z0-9]{1,50}/;
    const ExpreTest = new RegExp(expresion);
    const test = ExpreTest.test(val);

    if (test) {
      this.setState({ fancy_name_validation: true });
    } else {
      this.setState({ fancy_name_validation: false });
    }
  };

  render() {
    const {
      check_fancy,
      check_social,
      loading,
      fancy_name_validation,
      social_name_validation
    } = this.state;

    const { socialName, fancyName } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBack navigation={this.props.navigation} />
        <TitleRegister title={"Informe a razão social e o nome fantasia"} />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {check_fancy || check_social ? (
            <View style={BaseStyle.textValidationContainer}>
              <Text style={BaseStyle.textValidation}>
                Razão social ou nome fantasia já cadastrados
              </Text>
            </View>
          ) : null}
          <TextInput
            value={socialName}
            onChangeText={val => this.onValidationSocialName(val)}
            style={BaseStyle.input}
            placeholder="Razão social"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={50}
          />
          <TextInput
            value={fancyName}
            onChangeText={val => this.onValidationFancyName(val)}
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder="Nome Fantasia"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={50}
          />
          <View style={BaseStyle.buttonContainer}>
            {fancy_name_validation && social_name_validation ? (
              <TouchableOpacity onPress={() => this.checkParams()}>
                <View>
                  {loading ? (
                    <ActivityIndicator size={40} color={"#EE6E73"} />
                  ) : (
                    <Text style={BaseStyle.button}>Avançar</Text>
                  )}
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForm />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { socialName, fancyName } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    socialName: socialName,
    fancyName: fancyName,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialNameScreen);
