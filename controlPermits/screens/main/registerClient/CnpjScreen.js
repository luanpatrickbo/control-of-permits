import React, { PureComponent } from "react";
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { TextInputMask } from "react-native-masked-text";
import { BaseStyle } from "../../../styles/base";
import Api from "../../../utils/api";
import { setValue } from "../../../actions/valuesActions";
import TitleRegister from "../../components/TitleRegister";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForm from "../../components/OffLineForms";

class CnpjScreen extends PureComponent {
  state = {
    check_cnpj: false,
    loading: false,
    cnpj_validation: false,
    responsible_validation: false
  };

  checkCnpj = async () => {
    const { cnpj, isConnect, navigation } = this.props;
    const { check_cnpj } = this.state;

    this.setState({ loading: true });
    try {
      if (isConnect) {
        const result = await Api.checkCnpj(cnpj);
        this.setState({ check_cnpj: result });
        if (!check_cnpj) {
          navigation.navigate("stackAdress");
          this.setState({ loading: false });
        }
      }
      this.setState({ loading: false });
    } catch (error) {
      alert("tente novamente mais tarde");
    }
  };

  // Present Validation

  onValidationCnpj = value => {
    this.props.setValue("cnpj", value);

    value = value.replace(/\D/g, "");

    const expression = /[0-9]{14}/;
    const testExpre = new RegExp(expression);
    const test = testExpre.test(value);

    if (test) {
      this.setState({ cnpj_validation: true });
    } else {
      this.setState({ cnpj_validation: false });
    }
  };

  onValidationResponsable = value => {
    this.props.setValue("responsibleName", value);

    const expression = /[a-zA-Z0-9]{1,38}/;

    const testExpre = new RegExp(expression);

    const test = testExpre.test(value);

    if (test) {
      this.setState({ responsible_validation: true });
    } else {
      this.setState({ responsible_validation: false });
    }
  };

  render() {
    const {
      check_cnpj,
      loading,
      cnpj_validation,
      responsible_validation
    } = this.state;

    const { cnpj, responsibleName } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title={"Agora o CNPJ e nome do responsavel"} />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {check_cnpj ? (
            <View style={BaseStyle.textValidationContainer}>
              <Text style={BaseStyle.textValidation}>CNPJ já cadastrado</Text>
            </View>
          ) : null}
          <TextInputMask
            style={BaseStyle.input}
            value={cnpj}
            refInput={ref => {
              this.input = ref;
            }}
            onChangeText={value => this.onValidationCnpj(value)}
            placeholder={`CNPJ (somente números)`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            type={"cnpj"}
            maxLength={18}
            keyboardType={"numeric"}
          />

          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            value={responsibleName}
            onChangeText={value => this.onValidationResponsable(value)}
            placeholder="Nome do responsavel"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid={"transparent"}
            autoCorrect={false}
            maxLength={40}
          />
          <View style={BaseStyle.buttonContainer}>
            {responsible_validation && cnpj_validation ? (
              <TouchableOpacity onPress={() => this.checkCnpj()}>
                {loading ? (
                  <ActivityIndicator size={40} color="#EE6E73" />
                ) : (
                  <Text style={BaseStyle.button}>Avançar</Text>
                )}
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForm />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { cnpj, responsibleName } = state.register;
  const { isConnect } = state.setNetInfo;

  return {
    cnpj: cnpj,
    responsibleName: responsibleName,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CnpjScreen);
