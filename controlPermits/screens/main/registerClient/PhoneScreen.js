import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { TextInputMask } from "react-native-masked-text";
import { setValue } from "../../../actions/valuesActions";
import { BaseStyle } from "../../../styles/base";
import TitleRegister from "../../components/TitleRegister";
import ArrowBackwithExit from "../../components/ArrowBackwithExit";
import OffLineForms from "../../components/OffLineForms";

class PhoneScreen extends Component {
  state = {
    phone_validation: false,
    cell_phone_validation: false
  };

  // Validation present

  onvalidationPhone = value => {
    this.props.setValue("phone", value);

    value = value.replace(/\D/g, "");

    const expression = /[0-9]{10,11}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ phone_validation: true });
    } else {
      this.setState({ phone_validation: false });
    }
  };

  onvalidationCellPhone = value => {
    this.props.setValue("cellPhone", value);

    value = value.replace(/\D/g, "");

    const expression = /[0-9]{10,11}/;
    const testExpression = new RegExp(expression);
    const test = testExpression.test(value);

    if (test) {
      this.setState({ cell_phone_validation: true });
    } else {
      this.setState({ cell_phone_validation: false });
    }
  };

  render() {
    const { cell_phone_validation, phone_validation } = this.state;
    const { phone, cellPhone } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="mainScreen"
        />
        <TitleRegister title="Telefones para contato" />
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInputMask
            value={phone}
            onChangeText={value => this.onvalidationPhone(value)}
            style={BaseStyle.input}
            placeholder={`Digite o telefone ou celular`}
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            maxLength={15}
            autoCorrect={false}
            keyboardType="numeric"
            type={"cel-phone"}
            // options={{
            //   dddMask: "(99) 99999-9999"
            // }}
          />
          <TextInputMask
            value={cellPhone}
            onChangeText={value => this.onvalidationCellPhone(value)}
            style={[BaseStyle.input, { marginTop: 20 }]}
            placeholder={`Digite o telefone ou celular`}
            underlineColorAndroid="transparent"
            placeholderTextColor={BaseStyle.placeholderStyle}
            maxLength={15}
            keyboardType="numeric"
            type={"cel-phone"}
            // options={{
            //   dddMask: "(99) 99999-9999"
            // }}
          />
          <View style={BaseStyle.buttonContainer}>
            {phone_validation &&
            cell_phone_validation &&
            phone !== cellPhone ? (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("StackPermit")}
              >
                <Text style={BaseStyle.button}>Avançar</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity disabled={true}>
                <Text
                  style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                >
                  Avançar
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    phone: state.register.phone,
    cellPhone: state.register.cellPhone
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhoneScreen);
