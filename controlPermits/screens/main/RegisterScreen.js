import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import { connect } from "react-redux";
import moment from "moment";
import { getAllClients } from "../../actions/clientActions";
import { BaseStyle } from "../../styles/base";
import Load from "../components/Loading";
import { Entypo } from "@expo/vector-icons";
import Header from "../components/Header";
import OffLineForms from "../components/OffLineForms";

class ListClient extends PureComponent {
  state = {
    loading: true
  };

  async componentDidMount() {
    const { isConnect, loadClients } = this.props;
    try {
      if (isConnect) {
        await loadClients();
      }
    } catch (error) {
      alert("Desculpe algo acontceu de errado tente novamente");
    }

    this.setState({ loading: false });
  }

  componentDidUpdate(prevProps) {
    const { clients, isConnect } = this.props;
    if (JSON.stringify(prevProps.clients) !== JSON.stringify(clients)) {
      this.props.loadClients();
    }
    if (JSON.stringify(prevProps.isConnect) !== JSON.stringify(isConnect)) {
      this.props.loadClients();
    }
  }

  render() {
    const { clients } = this.props;
    const { loading } = this.state;

    if (loading) {
      return <Load />;
    }

    return (
      <View style={BaseStyle.container}>
        <Header />
        <OffLineForms />
        <View style={{ flex: 1 }}>
          {clients.length === 0 ? (
            <View style={BaseStyle.emptyContainer}>
              <Text style={BaseStyle.emptyTitle}>
                Você não possui clientes cadastrados
              </Text>
            </View>
          ) : (
              <ScrollView showsVerticalScrollIndicator={false} pagingEnabled>
                {clients.map(item => (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("StackInfoClient", {
                        id: item.id
                      })
                    }
                    key={item.id}
                    style={BaseStyle.item}
                  >
                    {item.permits.map(permit => (
                      <View key={permit.id} style={BaseStyle.listContainer}>
                        <Text style={BaseStyle.itemTitle}>
                          {permit.kind_id === 1
                            ? "Corpo de Bombeiros"
                            : "Licença"}
                          :
                      </Text>
                        <Text style={BaseStyle.itemTitle}>
                          {moment(permit.shelf_life).format("DD/MM/YYYY")}
                        </Text>
                      </View>
                    ))}
                    <View style={BaseStyle.listContainer}>
                      <Text style={BaseStyle.FontInfo}>Razão Social:</Text>
                      <Text
                        style={BaseStyle.infoClient}
                        numberOfLines={3}
                        ellipsizeMode="tail"
                      >
                        {item.social_name}
                      </Text>
                    </View>
                    <View style={BaseStyle.listContainer}>
                      <Text style={BaseStyle.FontInfo}>Nome Fantasia:</Text>
                      <Text
                        style={BaseStyle.infoClient}
                        numberOfLines={3}
                        ellipsizeMode="tail"
                      >
                        {item.fancy_name}
                      </Text>
                    </View>
                    <View style={BaseStyle.listContainer}>
                      <Text style={BaseStyle.FontInfo}>CNPJ:</Text>
                      <Text style={BaseStyle.infoClient}>
                        {item.cnpj.replace(
                          /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
                          "$1.$2.$3/$4-$5"
                        )}
                      </Text>
                    </View>
                    {item.phones.map(phone => (
                      <View style={BaseStyle.listContainer} key={phone.id}>
                        <Text style={BaseStyle.FontInfo}>
                          {phone.number.length === 11 ? "Celular" : "Telefone"}
                        </Text>
                        <Text style={BaseStyle.infoClient}>
                          {phone.number.length === 11
                            ? phone.number.replace(
                              /(\d{2})(\d{5})(\d{4})/,
                              "$1 $2-$3"
                            )
                            : phone.number.replace(
                              /(\d{2})(\d{4})(\d{4})/,
                              "$1 $2-$3"
                            )}
                        </Text>
                      </View>
                    ))}
                  </TouchableOpacity>
                ))}
              </ScrollView>
            )}
        </View>
        <TouchableOpacity
          style={BaseStyle.circleButton}
          onPress={() => this.props.navigation.navigate("StackSocialName")}
        >
          <View style={BaseStyle.circleButtonContainer}>
            <Entypo name="plus" size={25} color="#FFFFFF" />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { isConnect } = state.setNetInfo;
  const { clients } = state.client;
  return {
    clients: clients,
    isConnect: isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadClients: () => dispatch(getAllClients())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListClient);
