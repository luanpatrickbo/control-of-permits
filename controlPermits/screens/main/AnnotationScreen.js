import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import { Entypo } from "@expo/vector-icons";
import { BaseStyle } from "../../styles/base";
import { getAllAnnotations } from "../../actions/annotationActions";
import Load from "../components/Loading";
import Header from "../components/Header";
import OffLineForms from "../components/OffLineForms";

class annotationScreen extends Component {
  state = {
    loading: true
  };

  async componentDidMount() {
    const { loadAnnotations, isConnect } = this.props;
    try {
      if (isConnect) {
        await loadAnnotations();
      }
    } catch (error) {
      alert("Desculpe algo acontceu de errado tente novamente");
    }
    setTimeout(() => {
      this.setState({ loading: false });
    }, 400);
  }

  componentDidUpdate(prevProps) {
    const { annotations, isConnect, loadAnnotations } = this.props;
    if (JSON.stringify(prevProps.annotations) !== JSON.stringify(annotations)) {
      loadAnnotations();
    }
    if (JSON.stringify(prevProps.isConnect) !== JSON.stringify(isConnect)) {
      loadAnnotations();
    }
  }

  render() {
    const { annotations } = this.props;

    if (this.state.loading) {
      return <Load />;
    }
    return (
      <View style={BaseStyle.container}>
        <Header />
        <OffLineForms />
        <View style={{ flex: 1 }}>
          {annotations.length === 0 ? (
            <View style={BaseStyle.emptyContainer}>
              <Text style={BaseStyle.emptyTitle}>
                Você não possui anotações
              </Text>
            </View>
          ) : (
            <ScrollView showsVerticalScrollIndicator={false}>
              <FlatList
                data={annotations}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("StackInfoAnnotation", {
                          idAnnotation: item.id
                        })
                      }
                      style={BaseStyle.item}
                    >
                      <View>
                        <Text style={BaseStyle.itemTitle}>Anotação</Text>
                      </View>
                      <View style={BaseStyle.listContainer}>
                        <Text style={BaseStyle.FontInfo}>Descrição:</Text>
                        <Text
                          numberOfLines={5}
                          ellipsizeMode="tail"
                          style={BaseStyle.infoClient}
                        >
                          {item.description}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          )}
        </View>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("StackCreateAnnotation")
          }
          style={BaseStyle.circleButton}
        >
          <View style={BaseStyle.circleButtonContainer}>
            <Entypo name="plus" size={25} color="#FFFFFF" />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { annotations } = state.annotation;
  const { isConnect } = state.setNetInfo;

  return {
    annotations,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadAnnotations: () => dispatch(getAllAnnotations())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(annotationScreen);
