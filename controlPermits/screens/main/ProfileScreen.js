import React, { Component } from "react";
import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform
} from "react-native";
import { connect } from "react-redux";
import {
  Ionicons,
  EvilIcons,
  Feather,
  MaterialIcons
} from "@expo/vector-icons";
import { BaseStyle } from "../../styles/base";
import { loadProfile, logout } from "../../actions/profileAction";
import { ClientNumber } from "../../actions/clientActions";
import { annotationNumber } from "../../actions/annotationActions";
import Load from "../components/Loading";
import OffLine from "../components/OffLine";

class profileScreen extends Component {
  state = {
    loading: true,
    isConnect: false
  };

  async componentDidMount() {
    const { getProfile, ClientNumber, annotationNumber } = this.props;

    await Promise.all([getProfile(), ClientNumber(), annotationNumber()]);

    this.setState({ loading: false });
  }

  componentDidUpdate(prevProps) {
    const { clients, ClientNumber, annotations, annotationNumber } = this.props;

    if (JSON.stringify(prevProps.clients) !== JSON.stringify(clients)) {
      ClientNumber();
    }

    if (JSON.stringify(prevProps.annotations) !== JSON.stringify(annotations)) {
      annotationNumber();
    }
  }

  askLogout = () => {
    Alert.alert(
      "Você realmente quer sair?",
      `Fique mais um pouco :)`,
      [
        {
          text: "Cancelar",
          style: "cancel"
        },
        { text: "Sair", onPress: () => this.logout() }
      ],
      { cancelable: false }
    );
  };

  logout = async () => {
    const { logout, navigation } = this.props;
    await logout();
    navigation.navigate("StackLoginScreen");
  };

  render() {
    const { profile, clientsNumber, annotationsNumber } = this.props;

    if (this.state.loading) {
      return <Load />;
    }

    return (
      <View style={BaseStyle.container}>
        <View style={BaseStyle.headerBox}>
          <View style={BaseStyle.logoutContainer}>
            <TouchableOpacity onPress={() => this.askLogout()}>
              <Ionicons
                name={Platform.OS === "android" ? "md-exit" : "ios-exit"}
                color="#F7F7F7"
                size={30}
              />
            </TouchableOpacity>
          </View>
          <View style={BaseStyle.ProfileContainer}>
            <TouchableWithoutFeedback>
              <EvilIcons name={"user"} color="#F7F7F7" size={170} />
            </TouchableWithoutFeedback>
          </View>
          <View style={BaseStyle.nameContainer}>
            <Text style={BaseStyle.name}>{profile.user.name}</Text>
            <Text style={[BaseStyle.name, { marginLeft: 5 }]}>
              {profile.user.lastname}
            </Text>
          </View>
        </View>
        <OffLine>
          <View style={[BaseStyle.infomationsContainer, BaseStyle.section]}>
            {Platform.OS === "android" ? (
              <View style={{ marginRight: 5 }}>
                <MaterialIcons name="email" color="#EE5C51" size={25} />
              </View>
            ) : (
                <View style={{ marginRight: 5 }}>
                  <Ionicons name="ios-mail" color="#EE5C51" size={25} />
                </View>
              )}
            <Text
              style={[BaseStyle.infoTitle, { fontSize: 20, color: "#4a494a" }]}
            >
              E-mail:
            </Text>
            <Text style={[BaseStyle.info, { fontSize: 20 }]}>
              {profile.user.email}
            </Text>
          </View>

          <View style={[BaseStyle.infomationsContainer, BaseStyle.section]}>
            {Platform.OS === "android" ? (
              <View style={{ marginRight: 5 }}>
                <Feather name="user" color="#EE5C51" size={25} />
              </View>
            ) : (
                <View style={{ marginRight: 5 }}>
                  <Ionicons name="ios-person" color="#EE5C51" size={25} />
                </View>
              )}
            <Text
              style={[BaseStyle.infoTitle, { fontSize: 18, color: "#4a494a" }]}
            >
              Nome de Usuário:
            </Text>
            <Text style={[BaseStyle.info, { fontSize: 18 }]}>
              {profile.user.username}
            </Text>
          </View>

          <View style={[BaseStyle.infomationsContainer, BaseStyle.section]}>
            <View style={{ marginRight: 5 }}>
              <Ionicons
                name={Platform.OS === "android" ? "md-people" : "ios-people"}
                color="#EE5C51"
                size={25}
              />
            </View>
            <Text
              style={[BaseStyle.infoTitle, { fontSize: 18, color: "#4a494a" }]}
            >
              Meus Clientes:
            </Text>
            <Text style={[BaseStyle.info, { fontSize: 18 }]}>
              {clientsNumber}
            </Text>
          </View>

          <View style={[BaseStyle.infomationsContainer, BaseStyle.section]}>
            <View style={{ marginRight: 5 }}>
              <Ionicons
                name={Platform.OS === "android" ? "md-book" : "ios-bookmark"}
                color="#EE5C51"
                size={25}
              />
            </View>
            <Text
              style={[BaseStyle.infoTitle, { fontSize: 18, color: "#4a494a" }]}
            >
              Minhas Anotações:
            </Text>
            <Text style={[BaseStyle.info, { fontSize: 18 }]}>
              {annotationsNumber}
            </Text>
          </View>
        </OffLine>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { profile } = state.profile;
  const { clientsNumber, clients } = state.client;
  const { annotationsNumber, annotations } = state.annotation;

  return {
    profile,
    clientsNumber,
    clients,
    annotationsNumber,
    annotations
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProfile: () => dispatch(loadProfile()),
    logout: () => dispatch(logout()),
    ClientNumber: () => dispatch(ClientNumber()),
    annotationNumber: () => dispatch(annotationNumber())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(profileScreen);
