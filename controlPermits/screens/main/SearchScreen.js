import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { BaseStyle } from "../../styles/base";
import { Feather, Ionicons, Entypo } from "@expo/vector-icons";
import { searchClient, clearSearch } from "../../actions/clientActions";
import OffLine from "../components/OffLine";
import moment from "moment";

class searchScreen extends Component {
  state = {
    searchKey: "",
    empty: false,
    search: false,
    timeout: 0
  };

  clearState = () => {
    this.setState({ searchKey: "", empty: false });
    this.props.clearValues();
  };

  searchToClient = async query => {
    const { searchClient, isConnect } = this.props;
    if (isConnect) {
      await searchClient(query);
      if (this.props.clients.length === 0) {
        return this.setState({ empty: true, search: false });
      }
      return this.setState({ empty: false, search: false });
    }
    this.setState({ search: false });
  };

  search = query => {
    if (query.length === 0) {
      this.clearState();
    }
    if (this.state.timeout) {
      clearTimeout(this.state.timeout);
    }

    this.setState({
      search: query.trim().length > 0 ? true : false,
      searchKey: query,
      timeout: setTimeout(() => {
        if (query.trim().length > 0) {
          return this.searchToClient(query);
        }
        this.setState({ search: false });
      }, 600)
    });
  };

  render() {
    const { searchKey, search, empty } = this.state;

    const { clients } = this.props;

    return (
      <View style={BaseStyle.container}>
        <View style={BaseStyle.searchContainer}>
          <View style={BaseStyle.searchInputContainer}>
            <View style={BaseStyle.iconLeft}>
              <Feather name="search" size={20} color="#4a494a" />
            </View>
            <TextInput
              value={searchKey}
              style={{ width: 280 }}
              onChangeText={value => this.search(value)}
              placeholder="Pesquise por um cliente"
              placeholderTextColor={BaseStyle.placeholderStyle}
              underlineColorAndroid="transparent"
              maxLength={35}
            />
            {search ? (
              <View style={BaseStyle.iconRight}>
                <ActivityIndicator size={25} color="#EE6E73" />
              </View>
            ) : (
                <View style={BaseStyle.iconRight}>
                  <TouchableWithoutFeedback onPress={() => this.clearState()}>
                    <Ionicons
                      name={Platform.OS === "android" ? "md-close" : "ios-close"}
                      size={Platform.OS === "android" ? 25 : 30}
                      color="#4a494a"
                    />
                  </TouchableWithoutFeedback>
                </View>
              )}
          </View>
        </View>
        <OffLine>
          {searchKey.trim().length === 0 ? (
            <KeyboardAvoidingView
              style={BaseStyle.searchInfoContainer}
              behavior="padding"
            >
              <Text style={BaseStyle.searchInfo}>
                Pesquise por Razão Social, Nome Fantasia,
              </Text>
              <Text style={BaseStyle.searchInfo}>
                Nome do Responsável ou CNPJ
              </Text>
            </KeyboardAvoidingView>
          ) : (
              <ScrollView showsVerticalScrollIndicator={false}>
                {empty ? (
                  <KeyboardAvoidingView
                    style={{
                      marginTop: 180,
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Entypo name="emoji-sad" size={120} color="#242323" />
                    <Text style={{ fontSize: 15, marginTop: 15 }}>
                      Nenhum resultado encontrado.
                  </Text>
                  </KeyboardAvoidingView>
                ) : (
                    <View>
                      {clients.map(item => (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("StackInfoClient", {
                              id: item.id
                            })
                          }
                          key={item.id}
                          style={BaseStyle.item}
                        >
                          <View>
                            {item.permits.map(permit => (
                              <View key={permit.id} style={BaseStyle.listContainer}>
                                <Text style={BaseStyle.itemTitle}>Vencimento:</Text>
                                <Text style={BaseStyle.itemTitle}>
                                  {moment(permit.shelf_life).format("DD/MM/YYYY")}
                                </Text>
                              </View>
                            ))}
                            <View style={BaseStyle.listContainer}>
                              <Text style={BaseStyle.FontInfo}>Razão social:</Text>
                              <Text
                                style={BaseStyle.infoClient}
                                numberOfLines={3}
                                ellipsizeMode="tail"
                              >
                                {item.social_name}
                              </Text>
                            </View>
                            <View style={BaseStyle.listContainer}>
                              <Text style={BaseStyle.FontInfo}>Nome Fantasia:</Text>
                              <Text
                                style={BaseStyle.infoClient}
                                numberOfLines={3}
                                ellipsizeMode="tail"
                              >
                                {item.fancy_name}
                              </Text>
                            </View>
                            <View style={BaseStyle.listContainer}>
                              <Text style={BaseStyle.FontInfo}>CNPJ:</Text>
                              <Text style={BaseStyle.infoClient}>
                                {item.cnpj.replace(
                                  /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
                                  "$1.$2.$3/$4-$5"
                                )}
                              </Text>
                            </View>
                            {item.phones.map(phone => (
                              <View
                                style={BaseStyle.listContainer}
                                key={phone.number}
                              >
                                <Text style={BaseStyle.FontInfo}>Telefone:</Text>
                                <Text style={BaseStyle.infoClient}>
                                  {phone.number.length >= 11
                                    ? phone.number.replace(
                                      /(\d{2})(\d{5})(\d{4})/,
                                      "$1 $2-$3"
                                    )
                                    : phone.number.replace(
                                      /(\d{2})(\d{4})(\d{4})/,
                                      "$1 $2-$3"
                                    )}
                                </Text>
                              </View>
                            ))}
                          </View>
                        </TouchableOpacity>
                      ))}
                    </View>
                  )}
              </ScrollView>
            )}
        </OffLine>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { searchClient, clients } = state.client;
  const { isConnect } = state.setNetInfo;
  return {
    clients: searchClient,
    isConnect: isConnect
  };
};

const MapDispatchToProps = dispatch => {
  return {
    searchClient: query => dispatch(searchClient(query)),
    clearValues: () => dispatch(clearSearch())
  };
};

export default connect(
  mapStateToProps,
  MapDispatchToProps
)(searchScreen);
