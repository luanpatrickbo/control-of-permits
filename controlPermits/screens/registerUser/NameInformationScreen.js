import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { setValue, clearValues } from "../../actions/valuesActions";
import { BaseStyle } from "../../styles/base";
import { Ionicons } from "@expo/vector-icons";
import OffLineForms from "../components/OffLineForms";

class nameInformationScreen extends Component {
  state = {
    nameValidation: false,
    lastnameValidation: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons
            style={{ marginLeft: 15 }}
            name={
              Platform.OS === "ios" ? "ios-arrow-round-back" : "md-arrow-back"
            }
            size={Platform.OS === "ios" ? 40 : 25}
            color="#000"
          />
        </TouchableOpacity>
      ),

      headerStyle: {
        backgroundColor: "#F7F7F7",
        shadowOpacity: 0,
        shadowRadius: 0,
        shadowOffset: {
          height: 0
        },
        elevation: 0
      }
    };
  };

  onValidationName = value => {
    this.props.setValue("name", value);
    const expre = /^\S+$/;
    const expreTest = new RegExp(expre);
    const test = expreTest.test(value);

    if (test) {
      this.setState({ nameValidation: true });
    } else {
      this.setState({ nameValidation: false });
    }
  };

  onvalidationLastname = value => {
    this.props.setValue("lastName", value);
    const expre = /^\S+$/;
    const expreTest = new RegExp(expre);
    const test = expreTest.test(value);

    if (test) {
      this.setState({ lastnameValidation: true });
    } else {
      this.setState({ lastnameValidation: false });
    }
  };

  render() {
    const { nameValidation, lastnameValidation } = this.state;
    const { name, lastName } = this.props;

    return (
      <View style={BaseStyle.container}>
        <View style={BaseStyle.titleRegister}>
          <Text style={BaseStyle.title}>Qual o seu nome?</Text>
        </View>
        <KeyboardAvoidingView
          keyboardVerticalOffset={40}
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            style={BaseStyle.input}
            value={name}
            placeholder="Nome"
            placeholderTextColor={BaseStyle.placeholderStyle}
            onChangeText={value => this.onValidationName(value)}
            underlineColorAndroid="transparent"
            maxLength={38}
            autoCorrect={false}
          />
          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            value={lastName}
            placeholder="Sobrenome"
            placeholderTextColor={BaseStyle.placeholderStyle}
            onChangeText={val => this.onvalidationLastname(val)}
            underlineColorAndroid="transparent"
            maxLength={38}
            autoCorrect={false}
          />
          <View style={BaseStyle.buttonContainer}>
            {nameValidation && lastnameValidation ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("stackEmailInformation");
                }}
              >
                <View>
                  <Text style={BaseStyle.button}>Avançar</Text>
                </View>
              </TouchableOpacity>
            ) : (
                <TouchableWithoutFeedback disabled={true}>
                  <View>
                    <Text
                      style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                    >
                      Avançar
                  </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { name, lastName } = state.register;
  return {
    name,
    lastName
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(nameInformationScreen);
