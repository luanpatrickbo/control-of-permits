import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import { BaseStyle } from "../../styles/base";
import OffLineForms from "../components/OffLineForms";
import { connect } from "react-redux";
import { setValue, clearValues } from "../../actions/valuesActions";
import Api from "../../utils/api";
import ArrowBackwithExit from "../components/ArrowBackwithExit";


class usernameInformationScreen extends Component {
  state = {
    usernameValidation: false,
    passwordValidation: false,
    loading: false,
    checKUsername: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    };
  };

  // Validation Username

  onValidationUsername = value => {
    this.props.setValue("userName", value);

    const expresion = /^[a-zA-Z0-9]{4,32}$/;
    const expreTest = new RegExp(expresion);
    const test = expreTest.test(value);

    if (test) {
      this.setState({ usernameValidation: true });
    } else {
      this.setState({ usernameValidation: false });
    }
  };

  // Validation Password

  onValidationPassword = value => {
    this.props.setValue("password", value);

    const expre = /^[a-zA-Z0-9]{8,32}$/;
    const expreTest = new RegExp(expre);
    const test = expreTest.test(value);

    if (test) {
      this.setState({ passwordValidation: true });
    } else {
      this.setState({ passwordValidation: false });
    }
  };

  onRegisterUser = async () => {
    this.setState({ loading: true });

    const {
      name,
      lastName,
      email,
      userName,
      password,
      navigation,
      clearValues,
      isConnect
    } = this.props;
    try {
      if (isConnect) {
        const result = await Api.checkUsername(userName);
        this.setState({ checKUsername: result, loading: false });
        if (!this.state.checKUsername) {
          const resp = await Api.createUser({
            userName: userName,
            password: password,
            email: email,
            name: name,
            lastName: lastName
          });
          AsyncStorage.setItem("token", resp.token);
          clearValues();
          await this.setState({ loading: false });
          navigation.navigate("MainScreen");
        }
      }
      this.setState({ loading: false });
    } catch (error) {
      aler("tente novamente mais tarde");
    }
  };

  render() {
    const {
      usernameValidation,
      passwordValidation,
      loading,
      checKUsername
    } = this.state;

    const { userName, password } = this.props;
    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackLoginScreen"
        />
        <View style={BaseStyle.titleRegister}>
          <Text style={BaseStyle.title}>Estamos Quase Lá!!</Text>
        </View>
        <KeyboardAvoidingView
          keyboardVerticalOffset={20}
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {checKUsername ? (
            <View style={BaseStyle.titleContainer}>
              <Text style={BaseStyle.textValidation}>
                Nome de usuario já utilizado
              </Text>
            </View>
          ) : null}
          <TextInput
            style={BaseStyle.input}
            value={userName}
            onChangeText={value => this.onValidationUsername(value)}
            placeholder="Nome de usuário"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            autoCorrect={false}
            maxLength={32}
          />
          <TextInput
            style={[BaseStyle.input, { marginTop: 20 }]}
            value={password}
            onChangeText={value => this.onValidationPassword(value)}
            placeholder="Senha"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            secureTextEntry={true}
            maxLength={32}
          />
          <View style={BaseStyle.subtitleContainer}>
            <Text style={BaseStyle.subtitle}>
              Sua senha deve conter no minino 8 caractes entre letras e numeros
            </Text>
          </View>
          <View style={BaseStyle.buttonContainer}>
            {usernameValidation && passwordValidation ? (
              <TouchableOpacity onPress={() => this.onRegisterUser()}>
                <View>
                  {loading ? (
                    <ActivityIndicator size={40} color={"#EE6E73"} />
                  ) : (
                      <Text style={BaseStyle.button}>Cadastrar</Text>
                    )}
                </View>
              </TouchableOpacity>
            ) : (
                <TouchableWithoutFeedback disabled={true}>
                  <View>
                    <Text
                      style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                    >
                      Cadastrar
                  </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { name, lastName, email, userName, password } = state.register;
  const { isConnect } = state.setNetInfo;
  return {
    name,
    lastName,
    email,
    userName,
    password,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value)),
    clearValues: () => dispatch(clearValues())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(usernameInformationScreen);
