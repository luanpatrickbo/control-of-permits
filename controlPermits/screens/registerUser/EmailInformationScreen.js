import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  Platform
} from "react-native";
import { setValue } from "../../actions/valuesActions";
import { connect } from "react-redux";
import Api from "../../utils/api";
import { BaseStyle } from "../../styles/base";
import { Ionicons } from "@expo/vector-icons";
import OffLineForms from "../components/OffLineForms";
import ArrowBackwithExit from "../components/ArrowBackwithExit";


class emailInformationScreen extends Component {
  state = {
    check_email: false,
    emailValidation: false,
    loading: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    };
  };

  onvalidationEmail = value => {
    this.props.setValue("email", value);

    const expre = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})){1,38}$/;
    const expreTest = new RegExp(expre);
    const test = expreTest.test(value);

    if (test) {
      this.setState({ emailValidation: true });
    } else {
      this.setState({ emailValidation: false });
    }
  };

  checkEmail = async () => {
    const { isConnect, email, navigation } = this.props;

    this.setState({ loading: true });
    try {
      if (isConnect) {
        const result = await Api.checkEmail(email);
        this.setState({ check_email: result });
        if (this.state.check_email === false) {
          navigation.navigate("stackUsernameInfomation");
          this.setState({ loading: false });
        }
      }
      this.setState({ loading: false });
    } catch (error) {
      console.log(error);
      alert("tente novamente mais tarde");
    }
  };

  render() {
    const { emailValidation, check_email, loading } = this.state;

    const { email } = this.props;

    return (
      <View style={BaseStyle.container}>
        <ArrowBackwithExit
          navigation={this.props.navigation}
          route="StackLoginScreen"
        />
        <View style={[BaseStyle.titleRegister]}>
          <Text style={BaseStyle.title}>Agora Seu Email</Text>
        </View>
        <KeyboardAvoidingView
          keyboardVerticalOffset={20}
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          <TextInput
            style={BaseStyle.input}
            value={email}
            onChangeText={value => this.onvalidationEmail(value)}
            placeholder="E-mail"
            keyboardType="email-address"
            placeholderTextColor={BaseStyle.placeholderStyle}
            underlineColorAndroid="transparent"
            autoCorrect={false}
            maxLength={40}
          />
          <View style={BaseStyle.subtitleContainer}>
            {check_email ? (
              <Text style={BaseStyle.textValidation}>
                Este e-mail ja está sendo utilizado por favor informe um novo
              </Text>
            ) : (
                <Text style={BaseStyle.subtitle}>
                  Por favor digite um email valido para que possamos entrar em
                  contato futuramente
              </Text>
              )}
          </View>
          <View style={BaseStyle.buttonContainer}>
            {emailValidation ? (
              <TouchableOpacity onPress={() => this.checkEmail()}>
                <View>
                  {loading ? (
                    <ActivityIndicator size={40} color={"#EE6E73"} />
                  ) : (
                      <Text style={BaseStyle.button}>Avançar</Text>
                    )}
                </View>
              </TouchableOpacity>
            ) : (
                <TouchableWithoutFeedback disabled={true}>
                  <View>
                    <Text
                      style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                    >
                      Avançar
                  </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
          </View>
        </KeyboardAvoidingView>
        <OffLineForms />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { email } = state.register;
  const { isConnect } = state.setNetInfo;

  return {
    email,
    isConnect
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (key, value) => dispatch(setValue(key, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(emailInformationScreen);
