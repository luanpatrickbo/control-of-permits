import React, { Component } from "react";
import { AsyncStorage, ScrollView, Dimensions, View, Text } from "react-native";
import Steps from "./Steps";

class ApresentationScreen extends Component {
  static navigationOptions = {
    header: null
  };
  render() {
    const { width } = Dimensions.get("window");
    return (
      <ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            width: width
          }}
        >
          <Text>dlskjdslkjldksjldksjlskj</Text>
        </View>
        <Steps
          title="Vizualização rapida de clientes cadastrados"
          urlImage="https://s3-us-west-2.amazonaws.com/permits-control/FirstStep.jpg"
        />
        <Steps
          title="Pesquise seus clientes de uma forma pratica e rapida"
          urlImage="https://s3-us-west-2.amazonaws.com/permits-control/SecondStep.jpg"
        />
        <Steps
          title="Faça seu planejamento sem sair do app"
          urlImage="https://s3-us-west-2.amazonaws.com/permits-control/TrStep.jpg"
        />
        <Steps
          title="Visualize seus dados com facilidade"
          urlImage="https://s3-us-west-2.amazonaws.com/permits-control/FoStep.jpg"
        />

        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            width: width
          }}
        >
          <Text>dlskjdslkjldksjldksjlskj</Text>
        </View>
      </ScrollView>
    );
  }
}

export default ApresentationScreen;
