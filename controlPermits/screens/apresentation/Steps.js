import React, { Component } from "react";
import { View, Text, Image, Dimensions } from "react-native";
import { BaseStyle } from "../../styles/base";
import { Feather } from "@expo/vector-icons";

class Steps extends Component {
  render() {
    const { width } = Dimensions.get("window");

    const { title, urlImage } = this.props;
    return (
      <View
        style={[
          BaseStyle.container,
          { backgroundColor: "#FFFFFF", width: width }
        ]}
      >
        <View style={[BaseStyle.titleRegister, { marginTop: 30 }]}>
          <Text style={BaseStyle.title}>{title}</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View>
            <Feather name="chevron-left" size={40} style={{ marginLeft: 5 }} />
          </View>
          <Image
            style={{ height: 500, width: 250 }}
            source={{ uri: urlImage }}
          />
          <View>
            <Feather name="chevron-right" size={40} style={{ marginLeft: 5 }} />
          </View>
        </View>
        <View style={{ alignItems: "center", marginBottom: 8 }}>
          <Text style={{ fontSize: 12 }}>
            Arraste em uma das direções indicadas
          </Text>
        </View>
      </View>
    );
  }
}

export default Steps;
