import React, { Component } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
  AsyncStorage,
  Dimensions
} from "react-native";
import { BaseStyle } from "../styles/base";
class loginScreen extends Component {
  state = {
    username: "",
    usernameValidation: false,
    password: "",
    passwordValidation: false,
    loading: false,
    error: false
  };

  static navigationOptions = () => {
    return {
      header: null
    };
  };

  UNSAFE_componentWillMount() {
    this.setState({ loading: false });
    this.setState({ error: false });
  }

  // Validation Username

  onValidationUsername = val => {
    this.setState({ username: val });
    const expresion = /^[a-zA-Z0-9]{4,32}$/;
    const expreTest = new RegExp(expresion);
    const test = expreTest.test(val);

    if (test) {
      this.setState({ usernameValidation: true });
    } else {
      this.setState({ usernameValidation: false });
    }
  };

  // Validation Password

  onValidationPassword = val => {
    this.setState({ password: val });
    const expre = /^[a-zA-Z0-9]{8,32}$/;
    const expreTest = new RegExp(expre);
    const test = expreTest.test(val);

    if (test) {
      this.setState({ passwordValidation: true });
    } else {
      this.setState({ passwordValidation: false });
    }
  };

  // Login Request

  onLogin = () => {
    const { username, password } = this.state;

    this.setState({ loading: true });

    fetch("https://sleepy-springs-57790.herokuapp.com/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: username,
        password: password
      })
    })
      .then(res => res.json())
      .then(resp => {
        const keyToken = "token";

        if (resp.token) {
          AsyncStorage.setItem(keyToken, resp.token);

          this.props.navigation.navigate("MainScreen");
        } else {
          this.setState({ error: true });

          this.setState({ loading: false });
        }
      })
      .catch(err => {
        console.log(err.erro);
      });
    this.setState({ error: false });
  };

  render() {
    const {
      username,
      usernameValidation,
      password,
      passwordValidation,
      loading,
      error
    } = this.state;

    const { width } = Dimensions.get("window");

    return (
      <View style={BaseStyle.container}>
        <View
          style={[BaseStyle.titleContainer, { marginTop: 40, marginLeft: 95 }]}
        >
          <Text style={BaseStyle.title}>{`Alvarás Control`}</Text>
        </View>
        <Text>{this.state.message}</Text>
        <KeyboardAvoidingView
          behavior="padding"
          style={BaseStyle.formContainer}
        >
          {error ? (
            <View
              style={[BaseStyle.textValidationContainer, { marginBottom: 20 }]}
            >
              <Text style={BaseStyle.textValidation}>
                Usuário ou senha invalidos
              </Text>
            </View>
          ) : null}
          <TextInput
            style={BaseStyle.input}
            placeholder="Nome de usuário"
            placeholderTextColor={BaseStyle.placeholderStyle}
            value={username}
            onChangeText={val => this.onValidationUsername(val)}
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            maxLength={32}
          />
          <TextInput
            style={[{ marginTop: 20 }, BaseStyle.input]}
            placeholder="Senha"
            placeholderTextColor={BaseStyle.placeholderStyle}
            value={password}
            onChangeText={val => this.onValidationPassword(val)}
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            secureTextEntry={true}
            maxLength={32}
          />
          <View style={BaseStyle.registerContainer}>
            <Text style={BaseStyle.register}>Ainda não possui uma conta?</Text>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("stackNameInformation")
              }
            >
              <View>
                <Text
                  style={[
                    BaseStyle.register,
                    { marginLeft: 10, marginTop: 20, fontWeight: "600" }
                  ]}
                >
                  Cadastre-se
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={BaseStyle.buttonContainer}>
            {usernameValidation && passwordValidation ? (
              <TouchableOpacity onPress={() => this.onLogin()}>
                <View>
                  {loading ? (
                    <ActivityIndicator size={40} color={"#EE6E73"} />
                  ) : (
                    <Text style={BaseStyle.button}>Entrar</Text>
                  )}
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableWithoutFeedback>
                <View>
                  <Text
                    style={[BaseStyle.button, { backgroundColor: "#D46266" }]}
                  >
                    Entrar
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default loginScreen;
