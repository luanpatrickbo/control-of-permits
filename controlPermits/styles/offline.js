import React from "react";
import { StyleSheet } from "react-native";

export const OfflineStyle = StyleSheet.create({
  textContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  textColor: {
    color: "#4a494a",
    marginTop: 25,
    fontSize: 16
  }
});
