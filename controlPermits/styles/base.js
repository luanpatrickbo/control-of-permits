import React from "react";
import { StyleSheet } from "react-native";

export const BaseStyle = StyleSheet.create({
  // Base Style
  container: {
    flex: 1,
    backgroundColor: "#F7F7F7"
  },
  header: {
    height: 70,
    backgroundColor: "#FFFFFF",
    elevation: 3,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  textHeader: {
    paddingTop: 35,
    paddingLeft: 22,
    fontSize: 16,
    fontWeight: "600",
    color: "#4a494a"
  },
  trashContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: 29,
    marginRight: 6
  },
  titleRegister: {
    justifyContent: "flex-start",
    marginTop: 20,
    marginLeft: 20
  },
  title: {
    fontSize: 25,
    fontWeight: "600",
    color: "#4a494a"
  },
  emptyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  emptyTitle: {
    letterSpacing: 1,
    color: "#4a494a"
  },
  formContainer: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    padding: 10,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#F7F7F7",
    borderColor: "#4a494a",
    borderWidth: 0.5,
    overflow: "hidden"
  },
  pickerContainer: {
    borderColor: "#4a494a",
    borderWidth: 0.5,
    marginLeft: 20,
    marginRight: 20
  },
  datePicker: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 10,
    paddingLeft: 10,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#F7F7F7",
    color: "#4a494a",
    borderColor: "#4a494a",
    borderWidth: 0.5
  },
  placeholderStyle: {
    color: "#383738"
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50
  },
  button: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 40,
    paddingLeft: 40,
    backgroundColor: "#EE6E73",
    color: "#FFFFF6"
  },
  circleButton: {
    height: 55,
    width: 55,
    borderRadius: 100,
    backgroundColor: "#ee6e73",
    position: "absolute",
    bottom: 15,
    right: 15,
    elevation: 2
  },
  circleButtonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  subtitleContainer: {
    justifyContent: "center",
    paddingLeft: 20,
    paddingRight: 20
  },
  subtitle: {
    marginTop: 15,
    letterSpacing: 1,
    color: "#4a494a",
    fontSize: 12,
    alignItems: "flex-end"
  },
  registerContainer: {
    flexDirection: "row",
    alignItems: "flex-end",
    marginTop: 10,
    marginLeft: 35
  },
  register: {
    fontSize: 12,
    letterSpacing: 1,
    color: "#4a494a"
  },
  textValidationContainer: {
    alignItems: "center",
    justifyContent: "flex-start"
  },
  textValidation: {
    color: "#E30300",
    fontSize: 12,
    letterSpacing: 1,
    marginTop: 10,
    marginBottom: 5
  },

  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F7F7F7"
  },

  // Search Screen
  searchContainer: {
    height: 80,
    backgroundColor: "#F7F7F7",
    justifyContent: "center",
    elevation: 2
  },
  searchInputContainer: {
    flexDirection: "row",
    height: 40,
    width: 350,
    marginTop: 20,
    borderRadius: 8,
    elevation: 2,
    backgroundColor: "#FFFFFF",
    marginLeft: 5,
    marginRight: 5
  },
  iconLeft: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  iconRight: {
    marginTop: 7,
    marginRight: 10
  },
  searchInfoContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchInfo: {
    fontSize: 15,
    fontWeight: "600",
    color: "#4a494a"
  },

  // ANNOTATIONS ASK

  askContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  // INFOCLIENTES

  section: {
    marginTop: 16,
    borderBottomColor: "#D7D4D7",
    borderBottomWidth: 0.5
  },
  infoTitleContainer: {
    flex: 1,
    marginLeft: 10,
    flexDirection: "row"
  },
  infoTitle: {
    fontWeight: "500",
    flexWrap: "wrap",
    fontSize: 15
  },
  info: {
    marginLeft: 3,
    marginBottom: 20,
    fontSize: 15,
    fontWeight: "400",
    flexWrap: "wrap",
    color: "#4a494a"
  },
  titleContainer: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    marginBottom: 15
  },
  titleInformation: {
    fontSize: 20,
    fontWeight: "600",
    color: "#4a494a"
  },
  infoMapContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 20
  },
  editContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 15
  },

  // LISTCLIENTS

  listContainer: {
    flexDirection: "row"
  },
  item: {
    backgroundColor: "#FFFFFF",
    borderRadius: 8,
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 16,
    marginBottom: 5,
    justifyContent: "center",
    elevation: 3
  },
  itemTitle: {
    fontSize: 18,
    letterSpacing: 1,
    fontWeight: "500",
    marginBottom: 20,
    marginRight: 10,
    color: "#4a494a"
  },
  FontInfo: {
    fontWeight: "500",
    color: "#4a494a",
    fontSize: 16,
    marginRight: 10
  },
  infoClient: {
    flex: 1,
    flexWrap: "wrap",
    fontSize: 16,
    fontWeight: "400",
    color: "#4a494a",
    marginBottom: 12
  },

  // MAPSCREEN
  mapContainer: {
    flex: 1
  },
  maps: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: "absolute"
  },
  warningContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  warningText: {
    fontSize: 16,
    letterSpacing: 1,
    color: "#4a494a",
    marginTop: 30
  },

  // PROFILE SCREEN

  headerBox: {
    backgroundColor: "#EE5C51",
    height: 300
  },
  logoutContainer: {
    alignItems: "flex-end",
    marginTop: 30,
    marginRight: 15
  },
  infoBoxContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  infoBox: {
    width: 320,
    height: 230,
    position: "absolute",
    backgroundColor: "#FFFFFF",
    borderRadius: 8
  },
  myProfileContainer: {
    justifyContent: "flex-start",
    marginTop: 10,
    marginLeft: 30
  },
  myProfile: {
    fontSize: 25,
    color: "#FFFFFF",
    letterSpacing: 1,
    fontWeight: "600"
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  name: {
    fontSize: 25,
    fontWeight: "600",
    color: "#F7F7F7"
  },
  ProfileContainer: {
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 20
  },
  contentContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 21
  },
  content: {
    fontSize: 18,
    fontWeight: "600",
    letterSpacing: 1,
    color: "#969496"
  },
  values: {
    fontSize: 18,
    fontWeight: "600",
    color: "#4a494a"
  },
  valuesContainer: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  infomationsContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 25,
    marginLeft: 10,
    marginRight: 10
  }
});
