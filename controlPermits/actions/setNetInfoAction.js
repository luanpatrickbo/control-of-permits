import * as types from "./actionsTypes";

export function setNetInfo(connection) {
  return {
    type: types.NET_INFO,
    connection
  };
}
