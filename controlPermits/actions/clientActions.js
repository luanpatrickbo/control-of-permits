import * as types from "./actionsTypes";
import Api from "../utils/api";

function addClient(client) {
  return {
    type: types.ADD_CLIENT,
    client
  };
}

function loadClients(clients) {
  return {
    type: types.LOAD_CLIENT,
    clients
  };
}

function loadClient(client) {
  return {
    type: types.SHOW_CLIENT,
    client
  };
}

function alterClient(client) {
  return {
    type: types.EDIT_CLIENT,
    client
  };
}

function removeClient(client) {
  return {
    type: types.REMOVE_CLIENT,
    client
  };
}

function searchClients(clients) {
  return {
    type: types.SEARCH_CLIENT,
    clients
  };
}

function countClient(clientsNumber) {
  return {
    type: types.COUNT_CLIENTS,
    clientsNumber
  };
}

export function clearSearch() {
  return {
    type: types.CLEAR_SEARCH
  };
}

export function getAllClients() {
  return function(dispatch) {
    return Api.getAllClien().then(clients => {
      dispatch(loadClients(clients));
    });
  };
}

export function postClient({ ...values }) {
  return function(dispatch) {
    return Api.createClient(values).then(client => {
      dispatch(addClient(client));
    });
  };
}

export function showClient(id) {
  return function(dispatch) {
    return Api.getClient(id).then(client => {
      dispatch(loadClient(client));
    });
  };
}

export function deleteClient(id) {
  return function(dispatch) {
    return Api.deleteClien(id).then(client => {
      dispatch(removeClient(client));
    });
  };
}

export function editClient(id) {
  return function(dispatch) {
    return Api.editClien(id).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function editCompanie(id, { ...values }) {
  return function(dispatch) {
    return Api.editCompanieInfo(id, values).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function editAdres(id, { ...values }) {
  return function(dispatch) {
    return Api.editAdress(id, values).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function editPhones(id, phoneIds, { ...values }) {
  return function(dispatch) {
    return Api.editPhone(id, phoneIds, values).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function editAnnotationClient(id, annotationId, { ...values }) {
  return function(dispatch) {
    return Api.editAnnotationClient(id, annotationId, values).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function editPermits(id, permitIds, { ...values }) {
  return function(dispatch) {
    return Api.editPermits(id, permitIds, values).then(client => {
      dispatch(alterClient(client));
    });
  };
}

export function searchClient(query) {
  return function(dispatch) {
    return Api.searchClient(query).then(clients => {
      dispatch(searchClients(clients));
    });
  };
}

export function ClientNumber() {
  return function(dispatch) {
    Api.countClients().then(clientsNumber => {
      dispatch(countClient(clientsNumber));
    });
  };
}
