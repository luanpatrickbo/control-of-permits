import * as types from './actionsTypes'
import Api from '../utils/api';

function Profile(profile){
  return{
    type: types.LOAD_PROFILE,
    profile
  }
}

function deleteProfile(profile){
  return{
    type: types.LOGOUT_PROFILE,
    profile
  }
}

export function loadProfile(){
  return function(dispatch){
    return Api.loadtProfile().then((profile)=>{
      dispatch(Profile(profile))
    })
  }
}

export function logout(){
  return function(dispatch){
    return Api.logout().then((profile)=>{
      dispatch(deleteProfile(profile))
    })
  }
}