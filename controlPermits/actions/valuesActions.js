import * as types from './actionsTypes'

export function setValue(key, value){
  return{
    key: key,
    value: value,
    type: types.SET_VALUE,
  }
}

export function clearValues(){
  return{
    type: types.CLEAR_VALUES
  }
}