// Client
export const LOAD_CLIENT = "LOAD_CLIENT";
export const SHOW_CLIENT = "SHOW_CLIENT";
export const EDIT_CLIENT = "EDIT_CLIENT";
export const REMOVE_CLIENT = "REMOVE_CLIENT";
export const ADD_CLIENT = "ADD_CLIENT";
export const SEARCH_CLIENT = "SEARCH_CLIENT";
export const CLEAR_SEARCH = "CLEAR_SEARCH";
export const COUNT_CLIENTS = "COUNT_CLIENTS";
export const CLEAR_SHOWANNOTATIONS = "CLEAR_SHOWANNOTATIONS";

// Annotation
export const LOAD_ANNOTATIONS = "LOAD_ANNOTATIONS";
export const LOAD_ANNOTATION = "LOAD_ANNOTATION";
export const ADD_ANNOTATION = "ADD_ANNOTATION";
export const REMOVE_ANNOTATION = "REMOVE_ANNOTATION";
export const EDIT_ANNOTATION = "EDIT_ANNOTATION";
export const COUNT_ANNOTATIONS = "COUNT_ANNOTATIONS";

// Profile
export const LOAD_PROFILE = "LOAD_PROFILE";
export const LOGOUT_PROFILE = "LOGOUT_PROFILE";
export const CREATE_USER = "CREATE_USER";

// Kinds
export const LOAD_KINDS = "LOAD_KINDS";

// Values
export const SET_VALUE = "SET_VALUE";
export const CLEAR_VALUES = "CLEAR_VALUES";

// Geolocation
export const LOAD_GEOCODING = "LOAD_GEOCODING";

export const NET_INFO = "NET_INFO";
