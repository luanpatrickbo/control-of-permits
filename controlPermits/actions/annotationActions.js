import * as types from "./actionsTypes";
import Api from "../utils/api";

function loadAnnotations(annotations) {
  return {
    type: types.LOAD_ANNOTATIONS,
    annotations
  };
}

function loadAnnotation(annotation) {
  return {
    type: types.LOAD_ANNOTATION,
    annotation
  };
}

function addAnnotations(annotation) {
  return {
    type: types.ADD_ANNOTATION,
    annotation
  };
}

function alterAnnotation(annotation) {
  return {
    type: types.EDIT_ANNOTATION,
    annotation
  };
}

function deleteAnnotation(annotation) {
  return {
    type: types.REMOVE_ANNOTATION,
    annotation
  };
}

function countAnnotations(annotationsNumber) {
  return {
    type: types.COUNT_ANNOTATIONS,
    annotationsNumber
  };
}

export function clearShowAnnotation() {
  return {
    type: types.CLEAR_SHOWANNOTATIONS
  };
}

export function getAllAnnotations() {
  return function(dispatch) {
    return Api.getallAnnotation().then(annotations => {
      dispatch(loadAnnotations(annotations));
    });
  };
}

export function getAnnotation(id) {
  return function(dispatch) {
    return Api.getAnnoation(id).then(annotation => {
      dispatch(loadAnnotation(annotation));
    });
  };
}

export function postAnnotation(description) {
  return function(dispatch) {
    Api.createAnnotation(description).then(annotation => {
      dispatch(addAnnotations(annotation));
    });
  };
}

export function editAnnotation(id, description) {
  return function(dispatch) {
    return Api.editAnnotation(id, description).then(annotation => {
      dispatch(alterAnnotation(annotation));
    });
  };
}

export function removeAnnotation(id) {
  return function(dispatch) {
    return Api.removeAnnotation(id).then(annotation => {
      dispatch(deleteAnnotation(annotation));
    });
  };
}

export function annotationNumber() {
  return function(dispatch) {
    return Api.countAnnotations().then(AnnotationNumber => {
      dispatch(countAnnotations(AnnotationNumber));
    });
  };
}
