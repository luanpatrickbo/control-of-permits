import * as types from './actionsTypes'
import GeoCodingApi from '../utils/ geocodingApi'

function loadlGeocoding(results){
  return{
    type: types.LOAD_GEOCODING,
    results
  }
}

export function getAllGeocoding(address, number, city, state){
  return function(dispatch){
    return GeoCodingApi.getLocation(address, number, city, state)
    .then((results)=>{
      dispatch(loadlGeocoding(results))
    })
  }
}