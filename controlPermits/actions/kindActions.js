import * as types from './actionsTypes'
import Api from '../utils/api'

export function loadKinds(kinds){
  return {
    type: types.LOAD_KINDS,
    kinds
  }
}

export function getAllKinds(){
  return function(dispatch){
    return Api.getAllKinds().then((kinds)=>{
      dispatch(loadKinds(kinds))
    })
  }
}