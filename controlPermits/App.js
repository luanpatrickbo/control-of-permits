import React, { Component } from "react";
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator
} from "react-navigation";
import { NetInfo } from "react-native";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import { Ionicons, Feather } from "@expo/vector-icons";
import { setNotification } from "./utils/helpers";
import { setNetInfo } from "./actions/setNetInfoAction";

// Annotation

import ApresentationScreen from "./screens/apresentation/ApresentationScreen";

// Register user screen
import loginScreen from "./screens/loginScreen";
import NameInformationScreen from "./screens/registerUser/NameInformationScreen";
import EmailInformationScreen from "./screens/registerUser/EmailInformationScreen";
import UsernameInformationScreen from "./screens/registerUser/UsernameInformationScreen";

// Register client screen

import SocialNameScreen from "./screens/main/registerClient/SocialNameScreen";
import CnpjScreen from "./screens/main/registerClient/CnpjScreen";
import AdressScreen from "./screens/main/registerClient/AdressScreen";
import CepScreen from "./screens/main/registerClient/CepScreen";
import StateScreen from "./screens/main/registerClient/StateScreen";
import PhoneScreen from "./screens/main/registerClient/PhoneScreen";
import Annotation2Screen from "./screens/main/registerClient/Annotation2Screen";
import PermitScreen from "./screens/main/registerClient/PermitScreen";
import PermitScreen2 from "./screens/main/registerClient/PermiScreen2";
import EditCompanieInfo from "./screens/stackOptions/editCompanieInfo";
import EditCompanieCnpj from "./screens/stackOptions/editCompanieCnpj";
import EditAdress from "./screens/stackOptions/editAdress";
import EditCep from "./screens/stackOptions/editCep";
import EditState from "./screens/stackOptions/EditState";
import EditPhones from "./screens/stackOptions/editPhones";
import editAnnotationClient from "./screens/stackOptions/editAnnotationClient";
import editPermits from "./screens/stackOptions/editPermits";
import editPermits2 from "./screens/stackOptions/editPermit2";
import editAnnotation from "./screens/stackOptions/editAnnotation";

// Main Screen

import RegisterScreen from "./screens/main/RegisterScreen";
import SearchScreen from "./screens/main/SearchScreen";
import AnnotationScreen from "./screens/main/AnnotationScreen";
import ProfileScreen from "./screens/main/ProfileScreen";

// Info client and profile

import infoClientScreen from "./screens/stackOptions/infoClientScreen";
import MapScreen from "./screens/stackOptions/MapScreen";

// Annotation Screen

import createAnnotationScreen from "./screens/stackOptions/createAnnotationScreen";
import infoAnnotationScreen from "./screens/stackOptions/infoAnnotationScreen";

const store = configureStore();

export default class App extends Component {
  componentDidMount() {
    setNotification();
    NetInfo.addEventListener("connectionChange", connectionInfo => {
      store.dispatch(setNetInfo(connectionInfo.type !== "none"));
    });
  }

  render() {
    console.disableYellowBox = true;
    return (
      <Provider store={store}>
        <Swtch />
      </Provider>
    );
  }
}

const NavigateOptions = createBottomTabNavigator(
  {
    RegisterScreen: {
      screen: RegisterScreen,
      navigationOptions: {
        tabBarLabel: "CLIENTES",
        tabBarIcon: ({ tintColor }) => (
          <Feather name="users" size={25} color={tintColor} />
        )
      }
    },

    SearchScreen: {
      screen: SearchScreen,
      navigationOptions: {
        tabBarLabel: "PESQUISAR",
        tabBarIcon: ({ tintColor }) => (
          <Feather name="search" size={25} color={tintColor} />
        )
      }
    },

    AnnotationScreen: {
      screen: AnnotationScreen,
      navigationOptions: {
        tabBarLabel: "ANOTAÇÕES",
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="md-book" size={25} color={tintColor} />
        )
      }
    },

    ProfileScreen: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarLabel: "PERFIL",
        tabBarIcon: ({ tintColor }) => (
          <Feather name="user" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "#EE5C51",
      inactiveTintColor: "#616161",
      style: {
        backgroundColor: "#FFFFFF",
        elevation: 3,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: "black",
        shadowOpacity: 0.5
      }
    }
  }
);

const Main = createStackNavigator(
  {
    mainScreen: { screen: NavigateOptions },
    StackSocialName: { screen: SocialNameScreen },
    StackCnpj: { screen: CnpjScreen },
    stackAdress: { screen: AdressScreen },
    StackCep: { screen: CepScreen },
    StackState: { screen: StateScreen },
    StackPhone: { screen: PhoneScreen },
    StackAnnotation: { screen: Annotation2Screen },
    StackPermit: { screen: PermitScreen },
    StackPermit2: { screen: PermitScreen2 },
    StackInfoClient: { screen: infoClientScreen },
    StackMap: { screen: MapScreen },
    StackCreateAnnotation: { screen: createAnnotationScreen },
    StackInfoAnnotation: { screen: infoAnnotationScreen },
    StackEditCompanie: { screen: EditCompanieInfo },
    StackEditCompanieCnpj: { screen: EditCompanieCnpj },
    StackEditAdress: { screen: EditAdress },
    StackEditCep: { screen: EditCep },
    StackEditState: { screen: EditState },
    StackEditPhones: { screen: EditPhones },
    StackEditAnnotationClient: { screen: editAnnotationClient },
    StackEditPermits: { screen: editPermits },
    StackEditPermits2: { screen: editPermits2 },
    StackEditAnnotion: { screen: editAnnotation }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const RegisterUser = createStackNavigator({
  StackLoginScreen: loginScreen,
  stackNameInformation: NameInformationScreen,
  stackEmailInformation: EmailInformationScreen,
  stackUsernameInfomation: UsernameInformationScreen
});

const Swtch = createSwitchNavigator({
  RegisterScreen: RegisterUser,
  MainScreen: Main
});
