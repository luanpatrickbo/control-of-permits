import { AsyncStorage } from "react-native";

class Api {
  constructor(token) {
    this.token = token;
  }

  static async getAllClien() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/clients", {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async checkFancyName(fancyName) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/checkFancyName", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        fancy_name: fancyName
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async checkSocialName(socialName) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/checkSocialName", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        social_name: socialName
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async checkCnpj(cnpj) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/checkCnpj", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        cnpj: cnpj.replace(/\D/g, "")
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static checkEmail(email) {
    return fetch("https://sleepy-springs-57790.herokuapp.com/checkemail", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static checkUsername(username) {
    return fetch("https://sleepy-springs-57790.herokuapp.com/checkusername", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        username: username
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async getAllKinds() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/kinds", {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async createClient(values) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/clients", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        client: {
          fancy_name: values.fancyName,
          social_name: values.socialName,
          cnpj: values.cnpj.replace(/\D/g, ""),
          responsible_name: values.responsibleName,
          phones_attributes: [
            {
              number: values.phone.replace(/\D/g, "")
            },
            {
              number: values.cellPhone.replace(/\D/g, "")
            }
          ],
          address_attributes: {
            address_description: values.adress,
            number: values.number,
            city: values.city,
            neighborhood: values.neighborhood,
            state: values.states,
            cep: values.cep.replace(/\D/g, "")
          },
          permits_attributes: [
            {
              shelf_life: values.date,
              kind_id: values.kind
            },
            {
              shelf_life: values.date1,
              kind_id: values.kind1
            }
          ]
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async getClient(id) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async deleteClien(id) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editClien(id) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editCompanieInfo(id, values) {
    this.token = await AsyncStorage.getItem("token");
    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        client: {
          fancy_name: values.fancyName,
          social_name: values.socialName,
          cnpj: values.cnpj.replace(/\D/g, ""),
          responsible_name: values.responsibleName
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editAdress(id, values) {
    this.token = await AsyncStorage.getItem("token");
    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        client: {
          address_attributes: {
            id: id,
            address_description: values.adress,
            neighborhood: values.neighborhood,
            number: values.number,
            city: values.city,
            state: values.states,
            cep: values.cep.replace(/\D/g, "")
          }
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editPhone(id, phoneIds, values) {
    this.token = await AsyncStorage.getItem("token");
    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        client: {
          phones_attributes: [
            {
              id: phoneIds[0],
              number: values.phone.replace(/\D/g, "")
            },
            {
              id: phoneIds[1],
              number: values.cellPhone.replace(/\D/g, "")
            }
          ]
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editAnnotationClient(id, annotationId, values) {
    this.token = await AsyncStorage.getItem("token");
    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        client: {
          annotations_attributes: [
            {
              id: annotationId[0],
              description: values.annotation
            }
          ]
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editPermits(id, permitIds, values) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(`https://sleepy-springs-57790.herokuapp.com/clients/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        permits_attributes: [
          {
            id: permitIds[0],
            shelf_life: values.date,
            kind_id: values.kind
          },
          {
            id: permitIds[1],
            shelf_life: values.date1,
            kind_id: values.kind1
          }
        ]
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async countClients() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/countClients", {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  // SEARCH CLIENT

  static async searchClient(query) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/searchClients", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        keywords: query
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  // LOGIN

  static async loadtProfile() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/profile", {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async logout() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/logout", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static createUser({ ...values }) {
    return fetch("https://sleepy-springs-57790.herokuapp.com/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        user: {
          username: values.userName,
          password: values.password,
          email: values.email,
          name: values.name,
          lastname: values.lastName
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  // ANNOTATIONS

  static async getallAnnotation() {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/annotations", {
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      }
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async getAnnoation(id) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(
      `https://sleepy-springs-57790.herokuapp.com/annotations/${id}`,
      {
        headers: {
          "Content-Type": "application/json",
          token: this.token,
          Authorization: `Token ${this.token}`
        }
      }
    )
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async editAnnotation(id, description) {
    this.token = await AsyncStorage.getItem("token");
    return fetch(
      `https://sleepy-springs-57790.herokuapp.com/annotations/${id}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          token: this.token,
          Authorization: `Token ${this.token}`
        },
        body: JSON.stringify({
          annotation: {
            description: description
          }
        })
      }
    )
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async createAnnotation(description) {
    this.token = await AsyncStorage.getItem("token");

    return fetch("https://sleepy-springs-57790.herokuapp.com/annotations", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        token: this.token,
        Authorization: `Token ${this.token}`
      },
      body: JSON.stringify({
        annotation: {
          description: description
        }
      })
    })
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async countAnnotations() {
    this.token = await AsyncStorage.getItem("token");

    return fetch(
      "https://sleepy-springs-57790.herokuapp.com/countAnnotations",
      {
        headers: {
          "Content-Type": "application/json",
          token: this.token,
          Authorization: `Token ${this.token}`
        }
      }
    )
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }

  static async removeAnnotation(id) {
    this.token = await AsyncStorage.getItem("token");

    return fetch(
      `https://sleepy-springs-57790.herokuapp.com/annotations/${id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          token: this.token,
          Authorization: `Token ${this.token}`
        }
      }
    )
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
  }
}

export default Api;
