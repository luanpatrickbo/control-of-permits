class GeocodingApi{

  static getLocation(address, number, city, state){
    return fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}+${number}+${city}+${state}&key=AIzaSyDF4wnMHmYnFao8S6Ao-Qf6CaqsRBvloSw`,{
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then((res)=> res.json())
    .catch((error)=>{
      console.log(error)
      throw error
    })
  }
}

export default GeocodingApi