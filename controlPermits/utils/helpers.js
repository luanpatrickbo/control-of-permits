import React from "react";
import { AsyncStorage } from "react-native";
import { Permissions, Notifications } from "expo";

// NOTIFICATIONS

const NOTIFICATIONS_KEY = "PERMITSCONTROL:NOTIFICATION";

export function clearNotifications() {
  AsyncStorage.removeItem(NOTIFICATIONS_KEY).then(
    Notifications.cancelAllScheduledNotificationsAsync()
  );
}

export function createNotifications() {
  return {
    title: "CUIDADO COM A DATA DE VENCIMENTO!!",
    body: "Não esqueça de sempre acompanhar a data de vencimento dos álvaras",
    ios: {
      sound: true
    },
    android: {
      sound: true,
      priority: "hight",
      sticky: false,
      vibrate: true
    }
  };
}

export function setNotification() {
  AsyncStorage.getItem(NOTIFICATIONS_KEY)
    .then(JSON.parse)
    .then(data => {
      if (data === null) {
        Permissions.askAsync(Permissions.NOTIFICATIONS).then(({ status }) => {
          if (status === "granted") {
            Notifications.cancelAllScheduledNotificationsAsync();

            let tomorrow = new Date();

            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrow.setHours(10);
            tomorrow.setMinutes(0);

            Notifications.scheduleLocalNotificationAsync(
              createNotifications(),
              {
                time: tomorrow,
                repeat: "day"
              }
            );
            AsyncStorage.setItem(NOTIFICATIONS_KEY, JSON.stringify(true));
          }
        });
      }
    });
}
