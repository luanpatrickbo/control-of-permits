import * as types from "../actions/actionsTypes";

const initialState = {
  socialName: "",
  fancyName: "",
  cnpj: "",
  responsibleName: "",
  adress: "",
  neighborhood: "",
  number: "",
  cep: "",
  city: "",
  states: "Acre",
  phone: "",
  cellPhone: "",
  annotation: "",
  date: new Date(),
  date1: new Date(),
  kind: "",
  kind1: "",

  // Register User

  name: "",
  lastName: "",
  email: "",
  userName: "",
  password: ""
};

function register(state = initialState, action) {
  const { key, value } = action;

  switch (action.type) {
    case types.SET_VALUE:
      return {
        ...state,
        [key]: value
      };
    case types.CLEAR_VALUES:
      return {
        initialState
      };
    default:
      return state;
  }
}

export default register;
