import { combineReducers } from "redux";
import client from "./clientsReducers";
import kind from "./kindsReducers";
import register from "./valuesReducers";
import geocoding from "./geocodingReducer";
import profile from "./profileReducer";
import annotation from "./annotationReducers";
import setNetInfo from "./setNetInfoReducer";

export default combineReducers({
  client,
  kind,
  register,
  geocoding,
  profile,
  annotation,
  setNetInfo
});
