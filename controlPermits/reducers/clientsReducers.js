import * as types from "../actions/actionsTypes";

const initialState = {
  clients: [],
  clientsNumber: 0,
  searchClient: [],
  showClient: {
    phones: [],
    address: {
      address_description: "",
      cep: "",
      city: "",
      state: "",
      number: ""
    },
    permits: []
  }
};

function client(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_CLIENT: {
      const { clients } = action;
      return {
        ...state,
        clients: clients
      };
    }
    case types.ADD_CLIENT: {
      const { client } = action;
      return {
        ...state,
        clients: state.clients.concat([client])
      };
    }
    case types.SHOW_CLIENT: {
      const { client } = action;
      return {
        ...state,
        showClient: client
      };
    }
    case types.REMOVE_CLIENT: {
      const { client } = action;
      return {
        ...state,
        clients: state.clients.filter(c => c.id !== client.id),
        searchClient: state.searchClient.filter(c => c.id !== client.id)
      };
    }
    case types.EDIT_CLIENT: {
      const { client } = action;
      return {
        ...state,
        clients: state.clients.filter(c => c.id !== client.id).concat([client])
      };
    }
    case types.SEARCH_CLIENT: {
      const { clients } = action;
      return {
        ...state,
        searchClient: clients
      };
    }
    case types.CLEAR_SEARCH:
      return {
        ...state,
        searchClient: []
      };
    case types.COUNT_CLIENTS: {
      const { clientsNumber } = action;
      return {
        ...state,
        clientsNumber
      };
    }
    default:
      return state;
  }
}

export default client;
