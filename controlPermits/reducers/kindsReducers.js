import * as types from "../actions/actionsTypes";

const inititalState = {
  kinds: []
};

function kind(state = inititalState, action) {
  switch (action.type) {
    case types.LOAD_KINDS:
      return {
        ...state,
        kinds: action.kinds
      };
    default:
      return state;
  }
}

export default kind;
