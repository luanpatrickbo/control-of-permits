import * as types from "../actions/actionsTypes";

const initialState = {
  profile: {
    user: {
      username: "",
      email: "",
      name: "",
      lastname: ""
    }
  }
};

function profile(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_PROFILE: {
      const { profile } = action;
      return {
        ...state,
        profile: profile
      };
    }
    default:
      return state;
  }
}

export default profile;
