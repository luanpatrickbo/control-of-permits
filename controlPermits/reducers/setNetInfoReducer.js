import * as types from "../actions/actionsTypes";

const initialState = {
  isConnect: true
};

function setNetInfo(state = initialState, action) {
  switch (action.type) {
    case types.NET_INFO: {
      const { connection } = action;
      return {
        ...state,
        isConnect: connection
      };
    }
    default:
      return state;
  }
}

export default setNetInfo;
