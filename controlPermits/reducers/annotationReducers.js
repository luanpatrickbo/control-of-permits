import * as types from "../actions/actionsTypes";

const initialState = {
  annotations: [],
  annotationsNumber: 0,
  showAnnotation: {
    description: "",
    client_id: ""
  }
};

function annotation(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_ANNOTATIONS: {
      const { annotations } = action;
      return {
        ...state,
        annotations: annotations
      };
    }
    case types.LOAD_ANNOTATION: {
      const { annotation } = action;
      return {
        ...state,
        showAnnotation: annotation
      };
    }
    case types.ADD_ANNOTATION: {
      const { annotation } = action;
      return {
        ...state,
        annotations: state.annotations.concat([annotation])
      };
    }
    case types.EDIT_ANNOTATION: {
      const { annotation } = action;
      return {
        ...state,
        annotations: state.annotations
          .filter(a => a.id !== annotation.id)
          .concat([annotation])
      };
    }
    case types.COUNT_ANNOTATIONS: {
      const { annotationsNumber } = action;
      return {
        ...state,
        annotationsNumber
      };
    }
    case types.REMOVE_ANNOTATION: {
      const { annotation } = action;
      return {
        ...state,
        annotations: state.annotations.filter(a => a.id !== annotation.id)
      };
    }

    case types.CLEAR_SHOWANNOTATIONS: {
      return {
        ...state,
        showAnnotation: {
          description: "",
          client_id: ""
        }
      };
    }
    default:
      return state;
  }
}

export default annotation;
