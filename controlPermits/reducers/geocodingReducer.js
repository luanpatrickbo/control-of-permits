import * as types from "../actions/actionsTypes";

const initialState = {
  results: {
    results: []
  }
};

function geocoding(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_GEOCODING: {
      const { results } = action;
      return {
        ...state,
        results: results
      };
    }

    default:
      return state;
  }
}

export default geocoding;
